#!/usr/bin/env python

########################################################
#       Request historic data from datastore.py        #
########################################################
#
#  - Values stored in psql 'Sensor_History' model
#  - Access Redis for sensor values at programable intervals
#  - Intervals are specific to each element, and stored in Redis
#      Redis Keys:
#      - <sensor_ID>interval	interval in seconds as float
#      - <sensor_ID>		current value as float
#
#  - Entries are accessed through thier 'Sensor' model foriegn key
#      psql Models:
#      - Hisoric:
#        - element, timestamp, value
#      - Element:
#        - elementid
#
#  - ZeroMQ DEALER binds tcp127.0.0.1:3030 accepts requests for data / commands
#      Data Request Format:
#      - ['get', [<elementIDs>], <first_time>, <last_time>, <datapointQty>]
#      Interval Update Format:
#      - [<set>, <elementID>, <seconds>]
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle, sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)


# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import Group, Sensor, SensorHistory
from django.db import reset_queries


cache = redis.Redis(host='localhost', port=6379, db=0)

# Make a list of all active sensors and fill with data storage times and intervals
sensors = []
for group in Group.objects.filter(active=True):
    logging.debug('found group: ' + group.name + ' in postgres')
    for sensor in Sensor.objects.filter(group=group).filter(active=True):
        sensor_id = sensor.ID
        logging.debug('found active sensor: ' + sensor_id + ', checking redis for data storage interval')
        sensors.append(sensor_id)
logging.debug('sensors = ' + str(sensors))

brokercom = context.socket(zmq.DEALER)
brokercom.setsockopt(zmq.RCVTIMEO, 10000)
brokercom.setsockopt(zmq.SNDTIMEO, 10000)
brokercom.setsockopt(zmq.SNDHWM, 1)
brokercom.connect('tcp://127.0.0.1:3007')

zpoller = zmq.Poller()
zpoller.register(historycom, zmq.POLLIN)

datapoints = 100
last_time = time.time()
first_time = last_time - 6000

for sensor in sensors:
    brokercom.send_multipart(['get', sensor, first_time, last_time, datapoints])
    logging.debug('sent request to datastore')
    response = brokercom_recv_multipart()
    logging.debug('response = ' + str(response))
























