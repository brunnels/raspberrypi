#!/usr/bin/env python

# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################

import sys, zmq, time, pickle

message = sys.argv[1].split(',')
print 'Sent to historycon: ' + str(message)

context = zmq.Context()
zcon = context.socket(zmq.REQ)
zcon.setsockopt(zmq.RCVTIMEO, 5000)
zcon.setsockopt(zmq.SNDTIMEO, 5000)
zcon.connect("tcp://127.0.0.1:3030")

zcon.send_multipart(message)

response = zcon.recv()
try:
    response = pickle.loads(response)
    print response
except:
    print response
