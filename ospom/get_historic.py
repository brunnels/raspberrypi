#!/usr/bin/env python

##############################################
# Get historic data, redis -> psql
#---------------------------------------------
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, Element, Historic
import redis, time, datetime
cache = redis.Redis(host='localhost', port=6379, db=0)

debug = 0
argnum = 0
for a in sys.argv:
    if a[0:2] == '--':
        if a[2:] == 'debug':
            debug = int(sys.argv[int(argnum)+1])
    argnum += 1

for group in Group.objects.filter(active=True):
    if debug > 1: print 'group: ' + group.name
    for element in Element.objects.filter(group=group):
        try:
            if debug: print datetime.datetime.now().ctime()
            if debug: print 'element = ' + str(element.elementid)
            val = cache.get(element.elementid)
            if val:
                if debug: print 'val = ' + val
                if debug: print 'element: ' + str(element.name) + ' value = ' + val
                h = Historic(element=element)
                h.timestamp = time.time()
                h.value = val
                h.save()
        except Exception,e:
            print datetime.datetime.now().ctime() + '    ' + str(e)
