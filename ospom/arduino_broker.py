#!/usr/bin/env python

##############################################
#  ZeroMQ threads communicate with arduinos  #
##############################################
#
#  -Control Commands, DEALER, 127.0.0.1:3008
#      -Command Format
#          -['', <command>]
#      -Examples
#          -['', 'exit']	end program
#      -Reply Format
#          -[<reply>]
#
#  -Arduino Commands, DEALER, 127.0.0.1:3007
#      -Command Format
#          -[<group_ID>, <command>]
#      -Examples
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -send serial request to arduino, receive response
#  -send response back over zmq
#
##############################################
# License                              
#
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle, json

import xmlrpclib
supervisor_xml = xmlrpclib.Server('http://localhost:9003/RPC2')

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

cache = redis.Redis(host='localhost', port=6379, db=0)

context = zmq.Context()

# Direct connection to Arduinos
arducom = context.socket(zmq.ROUTER)
arducom.setsockopt(zmq.RCVTIMEO, 3000)
arducom.setsockopt(zmq.SNDTIMEO, 3000)
arducom.setsockopt(zmq.SNDHWM, 20)
arducom.setsockopt(zmq.LINGER, 2000)
arducom.bind('tcp://127.0.0.1:3006')

# External requests for Arduinos
brokercom = context.socket(zmq.ROUTER)
brokercom.setsockopt(zmq.SNDTIMEO, 5000)
brokercom.setsockopt(zmq.RCVTIMEO, 5000)
brokercom.setsockopt(zmq.SNDHWM, 20)
brokercom.setsockopt(zmq.LINGER, 2000)
brokercom.bind('tcp://127.0.0.1:3007')

mainpoller = zmq.Poller()
mainpoller.register(arducom, zmq.POLLIN)
mainpoller.register(brokercom, zmq.POLLIN)

# Start historic data storage and retrieval program
try:
    supervisor_xml.supervisor.stopProcess('datastore')
    logging.debug('restarting datastore supervisor process')
except:
    logging.debug('starting datastore supervisor process')
supervisor_xml.supervisor.startProcess('datastore')
logging.debug('started datastore')

# Stop previous arduino_worker processes
supervisor_xml.supervisor.stopProcess('arduino_worker:*')
logging.debug('stopped all arduino_worker supervisor processes')

# Start arduino_port_check
try:
    supervisor_xml.supervisor.stopProcess('arduino_port_check')
    logging.debug('restarting arduino_port_check supervisor process')
except:
    logging.debug('starting arduino_port_check supervisor process')
supervisor_xml.supervisor.startProcess('arduino_port_check')
logging.debug('started arduino_port_check')

while 1:
    socks = dict(mainpoller.poll(2000))
    
    if socks.get(brokercom) == zmq.POLLIN:
        try:
            recv_data = brokercom.recv_multipart()
            logging.info('brokercom: recv_data = ' + str(recv_data) + '  @ ' + str(time.time()))
            clientid = recv_data[0]
            logging.debug('brokercom: clientid = ' + str(clientid))
            groupid = recv_data[1]
            logging.debug('brokercom: groupid = ' + str(groupid))
            command = recv_data[2]
            logging.debug('brokercom: command = ' + str(command))
            web_client = recv_data[3]
            logging.debug('brokercom: web_client = ' + str(web_client))
            if groupid in cache.smembers('connected_groups'):
                pid = cache.get(groupid + '_pid')
                logging.debug('brokercom: pid = ' + str(pid))
                if pid != None:
                    arducom.send_multipart([pid, command, clientid, web_client])
                    logging.info('brokercom: sent to arducom: ' + str([pid, command, clientid, web_client]) + ' @ ' + str(time.time()) + '\n')
                else:
                    error_msg = 'group pid not found in redis'
                    brokercom.send_multipart([clientid, web_client, error_msg])
                    logging.info('brokercom: sent to client: ' + str([clientid, web_client, error_msg]) + ' @ ' + str(time.time()) + '\n')
            else:
                brokercom.send_multipart([clientid, 'ERROR, groupid: ' + groupid + ' not found!'])
                logging.error('brokercom: ERROR 1: groupid: ' + groupid + ' not found! \n')
        except Exception,e:
            logging.error('brokercom: ERROR 2: ' + str(e) + ' @ ' + str(time.time()) + '\n')
            brokercom.send_multipart([clientid, 'ERROR: ' + str(e)])
            
    if socks.get(arducom) == zmq.POLLIN:
        try:
            recv_data = arducom.recv_multipart()
            logging.info('arducom: recv_data = ' + str(recv_data) + '  @ ' + str(time.time()))
            groupid = recv_data[1]
            logging.debug('arducom: groupid = ' + str(groupid))
            data = recv_data[2]
            logging.debug('arducom: data = ' + str(data))
            clientid = str(recv_data[3])
            logging.debug('arducom: clientid = ' + str(clientid))
            web_client = recv_data[4]
            logging.debug('arducom: web_client = ' + str(web_client))
            brokercom.send_multipart([clientid, web_client, data])
            logging.info('arducom: sent to client: ' + str([clientid, web_client, data]) + ' @ ' + str(time.time()) + '\n')
        except Exception,e:
            logging.error('arducom: ERROR: ' + str(e) + '\n')
            brokercom.send_multipart([clientid, str(e)])
            logging.info('arducom: ERROR: sent to client: ' + str([clientid, data]) + ' @ ' + str(time.time()) + '\n')
    sys.stdout.flush()

