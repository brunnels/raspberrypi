#!/usr/bin/env python

##############################################
# Raspberry Pi Element Data Loader program   #
##############################################
#
#  -Arduino Commands, DEALER, 127.0.0.1:3006
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['0!']              get group ID
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -Loads element data into redis to set up the pins on the Ras Pi initially
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close, Gary Tomko
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import redis

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

cache = redis.Redis()

print "Initial Element Loader for a RasPi"
preprog = raw_input("Would you like to Load a pre-defined set of data into Redis for the Ras Pi Pins? (y/n)")
if preprog == 'y':
    pomtype = raw_input("ReefPOM or ChickenPOM? (type the name)")
    if pomtype == 'ReefPOM':
        cache.rpush('elementIDs', 'stfw0001')       # Element #1 ID
        cache.rpush('elementIDs', 'sfs00001')       # Element #2 ID
        cache.rpush('elementIDs', 'sfs00002')       # Element #3 ID
        cache.rpush('elementIDs', 'sdi00001')       # Element #4 ID
        cache.rpush('elementIDs', 'sdi00002')       # Element #5 ID
        cache.rpush('elementIDs', 'sdi00003')       # Element #6 ID
        cache.rpush('elementIDs', 'sdi00004')       # Element #7 ID
        cache.rpush('elementIDs', 'sdi00005')       # Element #8 ID
        cache.rpush('elementIDs', 'sdi00006')       # Element #9 ID
        cache.rpush('elementIDs', 'apw00007')       # Element #10 ID
        cache.rpush('elementIDs', 'apw00002')       # Element #11 ID
        
        # write stock Element Types
        # s=sensor, a=actuator, n=nothing, z=Loaded by this program
        cache.rpush('elementTypes', 's')        #Element #1 Type
        cache.rpush('elementTypes', 's')        #Element #2 Type
        cache.rpush('elementTypes', 's')        #Element #3 Type
        cache.rpush('elementTypes', 's')        #Element #4 Type
        cache.rpush('elementTypes', 's')        #Element #5 Type
        cache.rpush('elementTypes', 's')        #Element #6 Type
        cache.rpush('elementTypes', 's')        #Element #7 Type
        cache.rpush('elementTypes', 's')        #Element #8 Type
        cache.rpush('elementTypes', 's')        #Element #9 Type
        cache.rpush('elementTypes', 'a')        #Element #10 Type
        cache.rpush('elementTypes', 'a')        #Element #11 Type
        
        # Write stock Element Functions
        # 0=inactive, 1=analogRead, 2=digitalRead,
        # 3=analogWrite, 4=digitalWrite, 5=triac, 7=level, 8=custom sensor
        # 9=custom actuator, 10=Empty
        cache.rpush('elementFunctions', 8)        #Element #1 Function
        cache.rpush('elementFunctions', 2)        #Element #2 Function
        cache.rpush('elementFunctions', 2)        #Element #3 Function
        cache.rpush('elementFunctions', 0)        #Element #4 Function
        cache.rpush('elementFunctions', 0)        #Element #5 Function
        cache.rpush('elementFunctions', 0)        #Element #6 Function
        cache.rpush('elementFunctions', 0)        #Element #7 Function
        cache.rpush('elementFunctions', 0)        #Element #8 Function
        cache.rpush('elementFunctions', 0)        #Element #9 Function
        cache.rpush('elementFunctions', 3)        #Element #10 Function
        cache.rpush('elementFunctions', 3)        #Element #11 Function
        
        # Write stock Calibration data if the element is a sensor 
        # or Fail Safe Values / Extras they are actuators
        cache.rpush('elementSlopeFS', 1)        # Element #1 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #1 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #2 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #2 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #3 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #3 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #4 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #4 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #5 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #5 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #6 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #6 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #7 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #7 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #8 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #8 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #9 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #9 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #10 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #10 Y-Intercept / Extra
        cache.rpush('elementSlopeFS', 1)        # Element #11 Slope / FS Val
        cache.rpush('elementYintEX', 0)         # Element #11 Y-Intercept / Extra
        
        # Element Pins (Which Raspberry Pi pin they are associated with)
 ###  THIS NEEDS TO BE FILLED IN #####################################################################       
        cache.rpush('elementPins', 1)        # Element #1 Pin
        cache.rpush('elementPins', 1)        # Element #2 Pin
        cache.rpush('elementPins', 1)        # Element #3 Pin
        cache.rpush('elementPins', 1)        # Element #4 Pin
        cache.rpush('elementPins', 1)        # Element #5 Pin
        cache.rpush('elementPins', 1)        # Element #6 Pin
        cache.rpush('elementPins', 1)        # Element #7 Pin
        cache.rpush('elementPins', 1)        # Element #8 Pin
        cache.rpush('elementPins', 1)        # Element #9 Pin
        cache.rpush('elementPins', 1)        # Element #10 Pin
        cache.rpush('elementPins', 1)        # Element #11 Pin
        
        
    else:
        print "Not Yet Available"
else: 
    print "Not Yet Available"   
   
   
print "Element Data has been entered into Redis"
# Example:   temp_id = cache.get(system_id)

        
  
