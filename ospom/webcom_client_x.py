#!/usr/bin/python

# saved

#######################################
#        Web to Box connection        #
#######################################
# Persistent Zeromq REQ REP socket
# Web Server binds to 104.237.151.38:3010 with ROUTER socket
# Box connects to web server with DEALER socket
#
# Command List:
#  - ['historic', 'set', <elementID>, <updateInterval>]		set historic data update interval for specified element
#  - ['historic', 'get', [<elementIDs>], <firstTime>, <lastTime>, <datapoints>]	get historic data for specified element
#  - ['group', 'getall', <groupID>]	get sensor data from specified arduino group
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
######################################


import zmq, zmq.auth, redis, django, time, pickle, subprocess
from zmq.auth.thread import ThreadAuthenticator
import sys, logging
# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)

import xmlrpclib
supervisor_xml = xmlrpclib.Server('http://localhost:9003/RPC2')

import ipgetter
outter_ip = ipgetter.myip()

import base64

def pickle_message(msg, web_id):
    pickled_msg = pickle.dumps(msg)
    timenow = str(time.time())
    packet = {'msg': pickled_msg,
                    'webclientid': web_id}
    pickled_packet = pickle.dumps(packet)
    return pickled_packet

def load_message(msg):
    try:
        message = pickle.loads(msg)
    except Exception, e:
        message = msg
    return message

import socket 
def getNetworkIp():
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    soc.connect(('<broadcast>', 0))
    return soc.getsockname()[0]

local_ip = getNetworkIp()
logging.debug('local_ip = ' + str(local_ip))

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)
webcom_pid = str(os.getpid())
cache.set('webcom_pid', webcom_pid)
logging.debug('pid = ' + webcom_pid)

# Get System info from postgres
system = System.objects.filter(default_system=True)[0]
system_id = str(system.ID)
logging.debug('system_id = ' + system_id)
syspass = str(system.password)
logging.debug('syspass = ' + syspass)
reset_queries()

def update_elements(group_id, web_id):
    # Check if Group has already been registered with the system
    if group_id in Group.objects.all():
        logging.debug('found ' + group_id + ' in PostgreSQL, setting active flag')
        group = Group.objects.get(ID = group_id)
        group.active = True
        group.save()
        logging.info('group_id = ' + group_id)
        cache.sadd('connected_groups', group_id)
    else:
        # Register the new Group
        logging.debug(group_id + ' not found in dbgroups, creating new entry in psql')
        group = Group(ID = group_id)
        group.name = group_id
        group.active = True
        group.save()
        group.system.add(system)
        group.save()
        logging.info('group_id = ' + group_id)
        cache.sadd('connected_groups', group_id)
        try:
            logging.info('getting sensor IDs')
            arduino_broker.send_multipart([group_id, '12!', web_id])
            
            aresp = aresp.split('/')
            logging.debug('aresp = ' + str(aresp))
            for edat in aresp[1].split(','):
                edata = edat.split(':')
                logging.debug('edata = '+ str(edata))
                eid = edata[0]
                logging.debug('eid = ' + str(eid))
                edes = eid[1:3]
                logging.debug('edes = ' + str(edes))
                elval = edata[1]
                logging.debug('elval = ' + str(elval))
                if not Sensor.objects.filter(ID=eid).exists():
                    logging.debug('creating new database entry for Sensor: ' + eid)
                    e = Sensor(ID=eid)
                    try:
                        sensortype = SensorType.objects.get(des=edes)
                        logging.debug('found sensortype ' + edes + ' in database')
                        e.name = sensortype.name + ' ' + eid
                        e.etype = sensortype.name
                        e.eunits = sensortype.units
                        logging.debug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                        g = Group.objects.get(ID = mygroup)
                        e.group = g
                        e.save()
                    except:
                        logging.debug('sensortype ' + edes + ' NOT FOUND IN DATABASE')
                        logging.debug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                        g = Group.objects.get(ID = mygroup)
                        e.group = g
                        e.save()
                    
            logging.info('getting actuator IDs')        
            aresp = arduino_serial(ser, [mygroup, '14!'])
            aresp = aresp.split('/')
            if aresp[1] != '0':
                for edat in aresp[1].split(','):
                    eid = edat.split(':')[0]
                    edes = eid[1:4]
                    elval = edat.split(':')[1]
                    logging.debug('eid = ' + eid + ', val= ' + elval)
                    if not Actuator.objects.filter(ID=eid).exists():
                        logging.debug('creating new database entry for Actuator: ' + eid)
                        e = Actuator(ID=eid)
                        try:
                            actuatortype = ActuatorType.objects.get(des=edes)
                            logging.debug('found actuatortype ' + edes + ' in database')
                            e.name = actuatortype.name + ' ' + eid
                            e.etype = actuatortype.name
                            e.eunits = actuatortype.units
                            logging.debug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                            g = Group.objects.get(ID = mygroup)
                            e.group = g
                            e.save()
                        except:
                            logging.debug('actuatortype ' + edes + ' NOT FOUND IN DATABASE')
                            logging.debug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                            g = Group.objects.get(ID = mygroup)
                            e.group = g
                            e.save()
        
        except Exception, e:
            logging.error('New Group ERROR: ' + str(e) + ' @ ' + str(time.time()))
            logging.info('usb port ' + p + ' no longer connected')
            cache.srem('current_port_list', p)
            logging.debug('removed ' + p + ' from current_port_list in Redis')
            ##if Group.objects.filter(ID = mygroup).exists():
                ##Group.objects.get(ID = mygroup).delete()
                ##logging.error('Deleted Group: ' + mygroup)
            ser.close()
            cache.srem('connected_groups', mygroup)
            continue

import re
def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))
    
def login():
    global temp_id
    global temp_pass
    ctx = zmq.Context.instance()
    client = ctx.socket(zmq.DEALER)
    client.setsockopt(zmq.LINGER, 2000)
    client.setsockopt(zmq.RCVTIMEO, 10000)
    client.setsockopt(zmq.SNDTIMEO, 10000)
    # Connect to registration gateway
    ##client.connect('tcp://104.237.151.38:3010')
    client.connect('tcp://127.0.0.1:3010')
    logging.debug('connected to registration gateway @ tcp://104.237.151.38:3010')
    # Request new temporary ID and password
    msg = ['login', system_id, syspass, outter_ip, local_ip]
    client.send_multipart(msg)
    logging.debug('sent ' + str(msg))
    logging.debug('waiting for temp ID')
    # Verify ID and password are valid
    try:
        response = client.recv_multipart()
        logging.debug('response = ' + str(response))
        temp_id = response[0]
        temp_pass = response[1]
        server_ip = response[2]
        if len(temp_id) == 16 and len(temp_pass) == 16 and is_valid_ip(server_ip) :
            logging.info('temp_id = ' + temp_id + '\n' + 'temp_pass = ' + temp_pass)
            cache.set('temp_id', temp_id)
            cache.set('temp_pass', temp_pass)
            cache.set('server_ip', server_ip)
        else:
            logging.critical('registration ERROR1, response = ' + str(response) + ' Exiting.')
            sys.exit(0)
    except Exception, e:
        logging.critical('registration ERROR2 ' + str(e) + ' Exiting.')
        sys.exit(0)
        
def run():
    # Get temporary ID from ZMQ
    global temp_id
    logging.debug('temp_id = ' + temp_id)
    # Get system info from redis
    temp_pass = cache.get('temp_pass')
    logging.debug('temp_pass = ' + temp_pass)
    
    # ZeroMQ settings
    ctx = zmq.Context.instance()
    # Connection to Arduino Groups
    ##arduino_broker = ctx.socket(zmq.ROUTER)
    arduino_broker = ctx.socket(zmq.DEALER)
    arduino_broker.setsockopt(zmq.RCVTIMEO, 5000)
    arduino_broker.setsockopt(zmq.SNDTIMEO, 5000)
    arduino_broker.setsockopt(zmq.LINGER, 2000)
    arduino_broker.setsockopt(zmq.SNDHWM, 20)
    arduino_broker.setsockopt(zmq.RCVHWM, 20)
    arduino_broker.connect('tcp://127.0.0.1:3007') # Stunnel SSL
    # Historic data retrieval and settings 
    historycom = ctx.socket(zmq.REQ)
    historycom.setsockopt(zmq.RCVTIMEO, 5000)
    historycom.setsockopt(zmq.SNDTIMEO, 5000)
    historycom.setsockopt(zmq.LINGER, 2000)
    historycom.setsockopt(zmq.SNDHWM, 10)
    historycom.setsockopt(zmq.RCVHWM, 10)
    historycom.connect('tcp://127.0.0.1:3030')
    # Web connection to OSPOM server
    webcom = ctx.socket(zmq.DEALER)
    webcom.setsockopt(zmq.IDENTITY, temp_id)
    logging.debug('set client socket identity to ' + temp_id)
    webcom.setsockopt(zmq.LINGER, 2000)
    webcom.setsockopt(zmq.RCVTIMEO, 10000)
    webcom.setsockopt(zmq.SNDTIMEO, 10000)
    webcom.connect('tcp://127.0.0.1:3011')
    logging.debug('connected to tcp://104.237.151.38:3011')
    
    # Move group commands to an import file !!!!!!!!!!!!!!!!!!!!!!!!
    group_commands = {}
    group_commands['getall'] = '10!'
    group_commands['lowcal'] = '38!'
    group_commands['direct'] = 'direct'

    # Verify connection status
    msg = 'login'
    webcom.send_multipart(['0', pickle.dumps([temp_pass, msg])])
    logging.debug('logging in to server')
    
    resp = webcom.recv_multipart()
    logging.debug('resp = ' + str(resp))
    
    # Decode response
    response = load_message(resp[0])
    logging.debug('response = ' + str(response))
    msg = response[2]
    logging.debug('msg = ' + str(msg))
    time_stamp = response[1]
    
    if msg != 'LOGIN OK':
        logging.critical('webcom client Login ERROR!')
        sys.exit(0)
    else:
        logging.info('connected to OSPOM server')
    
    # Start filecom_client
    try:
        supervisor_xml.supervisor.stopProcess('filecom_client')
        logging.debug('restarting filecom_client supervisor process')
    except:
        logging.debug('starting filecom_clinet supervisor process')
    supervisor_xml.supervisor.startProcess('filecom_client')
    logging.debug('started filecom_client')
    
    # Start sensor_push
    try:
        supervisor_xml.supervisor.stopProcess('sensor_push')
        logging.debug('restarting sensor_push supervisor process')
    except:
        logging.debug('starting sensor_push supervisor process')
    supervisor_xml.supervisor.startProcess('sensor_push')
    logging.debug('started sensor_push')
    
    # Start image_push
    try:
        supervisor_xml.supervisor.stopProcess('image_push')
        logging.debug('restarting image_push supervisor process')
    except:
        logging.debug('starting image_push supervisor process')
    supervisor_xml.supervisor.startProcess('image_push')
    logging.debug('started image_push')
    
    poller = zmq.Poller()
    poller.register(webcom, zmq.POLLIN)
    poller.register(arduino_broker, zmq.POLLIN)
    poller.register(historycom, zmq.POLLIN)
    heartbeat = time.time()
    starttime = time.time()
    heartbeats_sent = 0
    heartbeat_check = True
    
    while True:
        socks = dict(poller.poll(100))
        ##logging.debug(str(time.time()))
        if socks.get(webcom) == zmq.POLLIN:
            try:
                # handle incoming messages from OSPOM server
                # Message format: [<web client ID>, <time stamp>, <message>]
                pickled_packet = webcom.recv_multipart()
                logging.debug('webcom:   pickled_packet = ' + str(pickled_packet))
                packet = load_message(pickled_packet[0])
                logging.debug('webcom:   packet = ' + str(packet))
                web_client = packet[0]
                time_stamp = packet[1]
                message = packet[2]
                logging.debug('webcom:   message = ' + str(message))
                try:
                    request = message[1:]
                    request.insert(0, web_client)
                    logging.debug('webcom:   request = ' + str(request))
                except:
                    request = []
                # Process the message
                if message == 'PING' or message == 'ping':
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'PONG'])])
                    logging.debug('webcom:   PONG')
                elif message == 'HEARTBEAT OK':
                    heartbeat_check = True
                # Request for historic data, or to set data storage interval
                elif message[0] == 'historic':
                    historycom.send(pickle.dumps(request))
                elif message[0] == 'system':
                    if message[1] == 'set_sensor_update':
                        cache.set('sensor_update_interval', message[2])
                        logging.debug('webcom:   set sensor update interval to ' + str(message[2]))
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps('set sensor update interval to ' + str(message[2]))])])
                # Requests for Arduino Group info
                elif message[0] == 'group':
                    # Add the web client ID to the message for response routing
                    action = request[1]
                    logging.debug('webcom:   action = ' + action)
                    if action == 'list_active_groups':
                        system = System.objects.get(ID=system_id)
                        group_ids = []
                        for group in Group.objects.filter(system=system).filter(active=True):
                            group_ids.append(str(group.ID))
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(group_ids)])])
                    elif action == 'list_active_sensors':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        sensor_ids = []
                        for sensor in Sensor.objects.filter(group=group).filter(active=True):
                            sensor_ids.append(sensor.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(sensor_ids)])])
                    elif action == 'list_all_sensors':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        sensor_ids = []
                        for sensor in Sensor.objects.filter(group=group):
                            sensor_ids.append(sensor.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(sensor_ids)])])
                    elif action == 'list_active_actuators':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        actuator_ids = []
                        for actuator in Actuator.objects.filter(group=group).filter(active=True):
                            actuator_ids.append(actuator.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(actuator_ids)])])
                    elif action == 'list_all_actuators':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        actuator_ids = []
                        for actuator in Actuator.objects.filter(group=group):
                            actuator_ids.append(actuator.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(actuator_ids)])])
                    elif action == 'direct':
                        command = request[2]
                        logging.debug('webcom:   command = ' + command)
                        group_id = request[3]
                        logging.debug('webcom:   group_id = ' + group_id)
                        arduino_broker.send_multipart([group_id, command, web_client])
                    reset_queries()
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('webcom:   poller ERROR! runtime = ' + runtime + '   ' + str(e))
                sys.exit(0)
        
        if socks.get(arduino_broker) == zmq.POLLIN:
            try:
                response = arduino_broker.recv_multipart()
                logging.debug('arduino_broker:   response = ' + str(response))
                web_client = response[0]
                logging.debug('arduino_broker:   web_client = ' + web_client)
                data = response[1]
                logging.debug('arduino_broker:   data = ' + data)
                webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(data)])])
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('arduino_broker:   poller ERROR! runtime = ' + runtime + '   ' + str(e))
                sys.exit(0)
                
        if socks.get(historycom) == zmq.POLLIN:
            try:
                packet = historycom.recv_multipart()
                logging.debug('historycom:   packet = ' + str(packet))
                web_client = packet[0]
                data = packet[1]
                webcom.send_multipart([web_client, pickle.dumps([temp_pass, data])])
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('historycom:   poller ERROR! runtime = ' + runtime + '   ' + str(e))
                sys.exit(0)
         
        if time.time() > heartbeat + 20:
            if heartbeat_check == False:
                break
            heartbeat = time.time()
            webcom.send_multipart(['1', pickle.dumps([temp_pass, 'HEARTBEAT'])])
            heartbeats_sent += 1
            logging.debug('heartbeat ' + str(heartbeats_sent))
            heartbeat_check = False
            



if __name__ == '__main__':
    if zmq.zmq_version_info() < (4,0):
        raise RuntimeError("Security is not supported in libzmq version < 4.0. libzmq version {0}".format(zmq.zmq_version()))
    #logging.basicConfig(level=level, format="[%(levelname)s] %(message)s")

    login()
    
    run()




 

'''
        while 1:
            socks = dict(mainpoller.poll(5000))
            if debug > 2: print time.time()
            if socks.get(webcom) == zmq.POLLIN:
                newmsg = webcom.recv_multipart()

                # Decode message
                if debug: print 'received: ' + str(newmsg)
                cipher = AES.new(temp_pass)
                encmsg = newmsg[0]
                try:
                    decmsg = DecodeAES(cipher, encmsg)
                    if debug > 1: print 'decodes to: ' + str(decmsg)
                except Exception,e:
                    print e
                    continue
                try:
                    indata = pickle.loads(decmsg)
                    if debug > 1: print 'unpickled: ' + str(indata)
                except Exception,e:
                    print e
                    continue
                    
            # Check message timestamp
            if debug: print 'indata = ' + str(indata)
            command = indata[0]
            if debug: print 'command = ' + str(command)
            webclientid = indata[1]
            timestamp = indata[2]
            lasttime = float(cache.get('webcomtime'))
            if (time.time() + 10) > timestamp > (time.time()-10) and lasttime != timestamp:
                if debug: print 'timestamp OK: ' + str(timestamp)
            else:
                print 'timestamp ERROR: ' + str(timestamp)
                continue
            ##print 'command = ' + str(command)
        
            codeprefix = command[0]
            # Handle commands
            if codeprefix == 'actuator':
                reqtype = command[1]
                print 'reqtype = ' + str(reqtype)
                if reqtype == 'getlist':     # Get active displayed Actuator list from database
                    try:
                        groups = Group.objects.filter(active=True)
                        elements = []
                        for g in groups:
                            elements.extend(Element.objects.filter(group = g))
                        print 'elements = ' + str(elements)
                        actlist = {}
                        for e in elements:
                            if e.elementid[0] == 'a' and e.order > 0 and e.active == True:
                                actlist[e.elementid] = [e.name, e.etype, e.eunits, e.order]
                        print 'actlist = ' + str(actlist)
                        pactlist = pickle.dumps(actlist)
                        timenow = str(time.time())
                        sendmsg = {'resp': pactlist,
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'sent: ' + str(encresp)
                    except Exception,e:
                        timenow = str(time.time())
                        sendmsg = {'resp': str(e),
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'ERROR sent: ' + str(encresp)
                        
                elif reqtype == 'getdata':
                    try:
                        elementids = command[3].replace('[', '').replace(']', '').split(',')
                        print 'elementids = ' + str(elementids)
                        groupids = []
                        for elementid in elementids:
                            if debug: print 'elementid = ' + elementid
                            element = Element.objects.get(elementid=elementid)
                            if debug: print 'element = ' + str(element)
                            group = element.group
                            if debug: print 'group = ' + str(group)
                            groupid = group.groupid
                            if debug: print 'groupid = ' + groupid
                            if groupid not in groupids:
                                if debug: print 'groupid ' + groupid + ' not found in groupids'
                                groupids.append(str(groupid))
                                if debug: print 'added ' + groupid + 'to groupids'
                            else:
                                if debug: print 'groupid ' + groupid + 'found in groupids'
                        if debug: print 'groupids = ' + str(groupids)
                        adata = []
                        for groupid in groupids:
                            arduino_broker.send_multipart([groupid, '13!'])
                            if debug > 1: print 'sent: ' + str([groupid, '13!']) + '   to arduino_broker'
                            output = arduino_broker.recv().split('/')
                            if debug: print 'output = ' + str(output)
                            adata.extend(output[1].split(','))
                        if debug: print 'adata = ' + str(adata)
                        padata = pickle.dumps(adata)
                        sendmsg = {'resp': padata,
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        print 'sendmsg = ' + str(sendmsg)
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'sent getdata: ' + str(encresp)
                    except Exception,e:
                        timenow = str(time.time())
                        sendmsg = {'resp': str(e),
                                   'timestamp': timenow,
                                   'webclientid': webclientid}
                        psend = pickle.dumps(sendmsg)
                        encresp = EncodeAES(cipher, psend)
                        webcom.send(encresp)
                        if debug: print 'ERROR sent: ' + str(encresp)
            
            elif codeprefix == 'group':
                reqtype = group_commands[command[1]]
                print 'reqtype = ' + str(reqtype)
                if reqtype == 'direct':     # Sent as a direct message to arduino group
                    dmsg = command[2]
                    groupid = dmsg[0]
                    cmd = dmsg[1]
                    arduino_broker.send_multipart([groupid, cmd])
                    print 'groupid = ' + str(groupid) + ', cmd = ' + str(cmd)
                else:
                    groupid = command[2]
                    print 'groupid = ' + str(groupid)
                    ##########################################################################
                    # Add more group command modifiers here !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    ##########################################################################
                    arduino_broker.send_multipart([groupid, reqtype])
                if debug: print 'sent: ' + str([groupid, reqtype]) + '   to arduino_broker'
                output = pickle.dumps(arduino_broker.recv_multipart())
                if debug: print 'received: ' + str(output) + '  from arduino_broker'
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)


            elif codeprefix == 'historic':
                try:
                    reqtype = command[1]
                    if reqtype == 'get':
                        elementids = pickle.dumps(command[2])
                        firsttime = command[3]
                        lasttime = command[4]
                        datapoints = command[5]
                        historycom.send_multipart([reqtype, elementids, firsttime, lasttime, datapoints])
                        if debug > 1: print 'sent: ' + str([reqtype, elementids, firsttime, lasttime, datapoints]) + '   to historycom'
                    elif reqtype == 'set':
                        ##elementid = pickle.dumps(command[2])
                        elementid = pickle.dumps(command[2])
                        updatetime = command[3]
                        historycom.send_multipart([reqtype, elementid, updatetime])
                        ##if debug > 1: print 'sent: ' + str([reqtype, elementid, updatetime]) + '   to historycom'
                    output = historycom.recv()
                    ##if debug > 1: print 'received: ' + str(output) + '  from historycom'
                    timenow = str(time.time())
                    sendmsg = {'resp': output,
                               'timestamp': timenow,
                               'webclientid': webclientid}
                    psend = pickle.dumps(sendmsg)
                    encresp = EncodeAES(cipher, psend)
                    webcom.send(encresp)
                    ##if debug: print 'sent: ' + str(encresp)
                except Exception,e:
                    timenow = str(time.time())
                    sendmsg = {'resp': 'webcom ERROR on historycom   ' + str(e),
                               'timestamp': timenow,
                               'webclientid': webclientid}
                    psend = pickle.dumps(sendmsg)
                    encresp = EncodeAES(cipher, psend)
                    webcom.send(encresp)
                    if debug: print str(e)
                    historycom.close()
                    historycom.connect('tcp://127.0.0.1:3030')

            elif codeprefix == 'run':
                runstring = command[1]
                output = subprocess.check_output(runstring, shell=True)
                if debug > 1: print 'received: ' + str(output) + '  from historycom'
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)

            else:
                output = 'ERROR: odroid received: ' + str(decmsg)
                timenow = str(time.time())
                sendmsg = {'resp': output,
                           'timestamp': timenow,
                           'webclientid': webclientid}
                psend = pickle.dumps(sendmsg)
                encresp = EncodeAES(cipher, psend)
                webcom.send(encresp)
                if debug: print 'sent: ' + str(encresp)
        sys.stdout.flush()


print 'exiting and closing all connections'
##webcom.close()
context.term()
print 'connections closed'
sys.exit(0)

'''
