#!/opt/ospom_env/bin/python

#######################################
#  Web to Box File Server connection  #
#######################################
# Persistent Zeromq DEALER > ROUTER
# Web Server binds to 104.237.151.38:3008 with ROUTER socket
# Box connects to web server with DEALER socket
#
# Command List:
#  - ['list_dirs', <base directory>]      returns a list of all directories recurcivly in the base directory
#  - ['list_files', <base directory>]     returns a list of all files recursivly in the base directory
#  - ['get_file_info', <file path>]       returns [<file length>, <file hash>] of the file at the given path
#  - ['get_file_chunk', <file directory>, <file name>, <chunk size>, <chunk number>
#                                         returns [<chunk number>, <chunk data>, <chunk hash>

#  - ['delete_file', <file path>]	      deletes the file at the given path
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, django, time, pickle, shutil

import ipgetter
outter_ip = ipgetter.myip()

from Crypto.Cipher import AES
import base64, hashlib

import sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level,
                    format='%(asctime)s %(message)s')

import xmlrpclib
supervisor_xml = xmlrpclib.Server('http://localhost:9003/RPC2')

def md5(fname):
    hash = hashlib.md5()
    with open(fname, "rb") as f:
        for i in iter(lambda: f.read(4096), b""):
            hash.update(i)
    return hash.hexdigest()

# Encryption Functions
PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

def encrypt_message(plain_msg, web_id):
    plain_pickle = pickle.dumps(plain_msg)
    timenow = str(time.time())
    plain_packet = {'msg': plain_pickle,
                    'webclientid': web_id}
    pickled_packet = pickle.dumps(plain_packet)
    cipher = AES.new(temp_pass)
    encoded_packet = EncodeAES(cipher, pickled_packet)
    return encoded_packet

def decrypt_message(encrypted_msg):
    plain_msg = ''
    cipher = AES.new(temp_pass)
    try:
        plain_msg = pickle.loads(DecodeAES(cipher, encrypted_msg))
    except Exception, e:
        plain_msg = DecodeAES(cipher, encrypted_msg)
    return plain_msg
    
def send_msg(soc, msg):
    cid = msg[0]
    timenow = str(time.time())
    packet = {'msg': msg[1], 'timestamp': timenow}
    pickled_packet = pickle.dumps(packet)
    cipher = AES.new(temp_id)
    encoded_packet = EncodeAES(cipher, pickled_packet)
    soc.send_multipart([cid, encoded_packet])
    logging.debug('sent message to client')

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)
filecom_pid = str(os.getpid())
cache.set('filecom_pid', filecom_pid)
logging.debug('pid = ' + filecom_pid)

# Get System info from postgres
s = System.objects.filter(default_system=True)[0]
sysid = str(s.ID)
logging.debug('sysid = ' + sysid)
syspass = str(s.password)
logging.debug('syspass = ' + syspass)
reset_queries()

import re
def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

temp_id = cache.get('temp_id')
temp_pass = cache.get('temp_pass')
logging.debug('temp_id = ' + temp_id + ' temp_pass = ' + temp_pass)
server_ip = cache.get('server_ip')
logging.debug('server_ip = ' + server_ip)

ctx = zmq.Context()

brokercom = ctx.socket(zmq.ROUTER)
brokercom.setsockopt(zmq.RCVTIMEO, 2000)
brokercom.setsockopt(zmq.SNDTIMEO, 20000)
brokercom.setsockopt(zmq.LINGER, 2000)
brokercom.setsockopt(zmq.SNDHWM, 10)
brokercom.setsockopt(zmq.RCVHWM, 10)
brokercom.bind('tcp://127.0.0.1:3037')

filecom = ctx.socket(zmq.DEALER)
filecom.setsockopt(zmq.IDENTITY, temp_id)
logging.debug('set client socket identity to ' + temp_id)
filecom.setsockopt(zmq.LINGER, 2000)
filecom.setsockopt(zmq.RCVTIMEO, 10000)
filecom.setsockopt(zmq.SNDTIMEO, 10000)
filecom.connect('tcp://127.0.0.1:3008')
logging.debug('connected to tcp://127.0.0.1:3008')

# Verify connection status
msg = 'login'
encmsg = encrypt_message(msg, '0')
filecom.send_multipart([encmsg])
logging.debug('logging in to server')

if os.system('ping -c 1 google.com') == 0:
    logging.debug('google.com ping OK')
else:
    logging.debug('google.com ping error, waiting 10 seconds, then restarting')
    time.sleep(10)
    logging.debug('restarting')
    sys.exit(0)

try:
    enc_resp = filecom.recv_multipart()
    logging.debug('enc_resp = ' + str(enc_resp))
except Exception, e:
    logging.debug('login ERROR ' + str(e) + ', restarting webcom_client')
    try:
        supervisor_xml.supervisor.stopProcess('webcom_client')
        logging.debug('restarting webcom_client supervisor process')
    except:
        logging.debug('starting webcom_clinet supervisor process')
    supervisor_xml.supervisor.startProcess('webcom_client')
    logging.debug('started webcom_client')
    sys.exit(0)

# Decode response
response = decrypt_message(enc_resp[0])
logging.debug('response = ' + str(response))
msg = response['msg']
time_stamp = response['timestamp']
    

poller = zmq.Poller()
poller.register(filecom, zmq.POLLIN)
poller.register(brokercom, zmq.POLLIN)
heartbeat = time.time()
heartbeat_timeout = heartbeat + 60
heartbeat_errors = 0
starttime = time.time()
messages_sent = 0


while True:
    socks = dict(poller.poll(100))
    ##logging.debug(str(time.time()))
    if socks.get(filecom) == zmq.POLLIN:
        try:
            # handle incoming messages from File Server
            encrypted_message = filecom.recv_multipart()
            ##logging.debug('encrypted_message = ' + str(encrypted_message))
            logging.debug('received encrypted message')
            message = decrypt_message(encrypted_message[0])
            ##logging.debug('message = ' + str(message))
            web_client = message['webclientid']
            time_stamp = message['timestamp']
            msg = pickle.loads(message['msg'])
            ##logging.debug('msg = ' + str(msg))
            if msg == ['PING'] or msg == ['ping']:
                encmsg = encrypt_message('PONG', web_client)
                filecom.send_multipart([encmsg])
                logging.debug('PONG')
                continue
            elif msg == ['HEARTBEAT OK']:
                logging.debug('HEARTBEAT OK')
                heartbeat_timeout = time.time() + 60
                heartbeat_errors = 0
                continue
                
        except Exception, e:
            logging.error('incoming packet ERROR! ' + str(e))
            continue
            
        command = msg[0]
        logging.debug('command = ' + str(command))
        
        ####################################################
        # File Upload Commands
        
        if command == 'copy_file':
            file_name = msg[1]
            logging.debug('creating temporaty file ' + file_name + '.temp')
            try:
                shutil.copy2(file_name, file_name + '.temp')
            except Exception, e:
                logging.error(e)
        
        
        elif command == 'get_file_chunk':
            file_dir = msg[1]
            logging.debug('file_dir = ' + file_dir)
            file_name = msg[2]
            logging.debug('file_name = ' + file_name)
            chunk_size = int(msg[3])
            logging.debug('chunk_size = ' + str(chunk_size))
            chunk_number = int(msg[4])
            logging.debug('chunk_number = ' + str(chunk_number))
            if os.path.isfile(file_dir + file_name):
                f = open(file_dir + file_name, "r")
                f.seek(chunk_number * chunk_size, os.SEEK_SET)
                data = f.read(chunk_size)
                chunk_hash = hashlib.md5(data).hexdigest()
                f.close()
                logging.debug('len(data) = ' + str(len(data)))
                encmsg = encrypt_message([command, [str(chunk_number), base64.b64encode(data), chunk_hash]], web_client)
                filecom.send_multipart([encmsg])
            else:
                encmsg = encrypt_message([command, ['get_file_chunk file path ERROR']], web_client)
                filecom.send_multipart([encmsg])


        elif command == 'get_file_info':
            file_dir = msg[1]
            logging.debug('file_dir = ' + file_dir)
            file_name = msg[2]
            logging.debug('file_name = ' + file_name)
            if os.path.isfile(file_dir + file_name):
                file_length = str(os.path.getsize(file_dir + file_name))
                logging.debug('file_lemgth = ' + file_length)
                file_hash = md5(file_dir + file_name)
                logging.debug('file_hash = ' + str(file_hash))
                encmsg = encrypt_message([command, [file_length, file_hash]], web_client)
                filecom.send_multipart([encmsg])
            else:
                encmsg = encrypt_message([command, ['get_file_info file path ERROR']], web_client)
                filecom.send_multipart([encmsg])
            

        elif command == 'list_files':
            dir_name = msg[1]
            logging.debug('dir_name = ' + dir_name)
            file_list = []
            for path, dirs, files in os.walk(dir_name):
                current_dir = '/' + path.replace(dir_name, '')
                logging.debug('current_dir = ' + current_dir)
                logging.debug('files = ' + str(files))
                for f in files:
                    file_list.append(current_dir + '/' + f)
            logging.debug('file_list = ' + str(file_list))
            encmsg = encrypt_message([command, file_list], web_client)
            filecom.send_multipart([encmsg])
            logging.info('sent file list to client ' + str(web_client))

        
        elif command == 'list_dirs':
            dir_name = msg[1]
            logging.debug('dir_name = ' + dir_name)
            dir_list = []
            for path, dirs, files in os.walk(dir_name):
                current_dir = path.replace(dir_name, '')
                logging.debug('current_dir = ' + current_dir)
                logging.debug('sub directories:')
                for d in dirs:
                    new_dir = current_dir + '/' + d + '/'
                    logging.debug('new_dir = ' + new_dir)
                    dir_list.append(new_dir)
                logging.debug('files:')
                for f in files:
                    logging.debug(f)
            logging.debug('dir_list = ' + str(dir_list))
            encmsg = encrypt_message([command, dir_list], web_client)
            filecom.send_multipart([encmsg])
            logging.info('sent directory list to client ' + str(web_client))
            
            
        elif command == 'delete_file':
            file_name = msg[1]
            logging.debug('deleting ' + file_name)
            try:
                os.remove(file_name)
            except Exception, e:
                logging.error(e)
                
        elif command == 'save_file':
            file_name = msg
            
        
        ##########################################################
        # File Download Commands
            
        elif command == 'put_file_hash':
            file_dir = msg[1]
            logging.debug('file_dir = ' + file_dir)
            file_name = msg[2]
            logging.debug('file_name = ' + file_name)
            file_hash = msg[3]
            logging.debug('file_hash = ' + file_hash)
            file_path = file_dir + file_name
            logging.debug('file_path = ' + file_path)
            if os.path.isfile(file_path):
                orig_hash = md5(file_path)
                logging.debug('orig_hash = ' + str(orig_hash))
                if orig_hash == file_hash:
                    encmsg = encrypt_message([command, ['HASH MATCH', file_dir, file_name, file_hash]], web_client)
                else:
                    encmsg = encrypt_message([command, ['HASH NEW', file_dir, file_name, file_hash]], web_client)
            else:
                encmsg = encrypt_message([command, ['HASH OK', file_dir, file_name, file_hash]], web_client)
            try:
                f = open(file_path + '.temp/' + 'md5sum', 'w')
                f.write(file_hash)
                f.close()
            except:
                encmsg = encrypt_message([command, ['HASH ERROR', file_dir, file_name, file_hash]], web_client)
            filecom.send_multipart([encmsg])
        
        
        elif command == 'put_dir':
            new_dir = msg[1]
            logging.debug('new_dir = ' + new_dir)
            try:
                if not os.path.exists(new_dir):
                    os.makedirs(new_dir)
                    encmsg = encrypt_message([command, ['DIR OK', new_dir]], web_client)
                    logging.debug('put_dir, creatine new directory ' + new_dir)
                else:
                    encmsg = encrypt_message([command, ['DIR EXISTS', new_dir]], web_client)
                logging.debug('put_dir, directory already exists ' + new_dir)    
                filecom.send_multipart([encmsg])
            except:
                logging.debug('put_dir, DIRECTORY ERROR ' + new_dir)
                encmsg = encrypt_message([command, ['DIR ERROR', new_dir]], web_client)
                filecom.send_multipart([encmsg])
            
        
        elif command == 'put_file_chunk':
            file_dir = msg[1]
            logging.debug('file_dir = ' + file_dir)
            file_name = msg[2]
            logging.debug('file_name = ' + file_name)
            chunk_data = base64.b64decode(msg[3])
            logging.debug('len(chunk_data) = ' + str(len(chunk_data)))
            chunk_number = msg[4]
            logging.debug('chunk_number = ' + str(chunk_number))
            chunk_hash = msg[5]
            logging.debug('chunk_hash = ' + chunk_hash)
            hash_check = hashlib.md5(chunk_data).hexdigest()
            logging.debug('hash_check = ' + hash_check)
            if hash_check == chunk_hash:
                logging.debug('chunk hash OK')
                ##encmsg = encrypt_message([command, ['HASH MATCH', file_dir, file_name, chunk_hash]], web_client)
                ##filecom.send_multipart([encmsg])
            else:
                encmsg = encrypt_message([command, ['HASH ERROR', file_dir, file_name, chunk_hash]], web_client)
                filecom.send_multipart([encmsg])
                continue
            
            file_path = file_dir + file_name + '.temp/' + str(chunk_number)
            logging.debug('file_path = ' + file_path)
            
            try:
                f = open(file_path, 'w')
                f.write(chunk_data)
                f.close()
                new_hash = md5(file_path)
                if new_hash == chunk_hash:
                    encmsg = encrypt_message([command, ['HASH OK', file_dir, file_name, chunk_hash, chunk_number]], web_client)
                    logging.debug('new_hash =  ' + new_hash)
                else:
                    encmsg = encrypt_message([command, ['HASH ERROR 2', file_dir, file_name, chunk_hash, chunk_number]], web_client)
                    logging.error('CHUNK HASH ERROR')
            except:
                encmsg = encrypt_message([command, ['HASH ERROR 3', file_dir, file_name, chunk_hash, chunk_number]], web_client)
            
            filecom.send_multipart([encmsg])
            
            
        elif command == 'put_file_complete':
            file_dir = msg[1]
            logging.debug('file_dir = ' + file_dir)
            file_name = msg[2]
            logging.debug('file_name = ' + file_name)
            total_chunks = msg[3]
            logging.debug('total_chunks = ' + str(total_chunks))
            file_path = file_dir + file_name
            # check number of file chunks against total_chunks
            chunk_number = 0
            for file in os.listdir(file_path + '.temp'):
                if file.isdigit():
                    chunk_number += 1
            logging.debug('chunk_number = ' + str(chunk_number))
            if chunk_number == total_chunks:
                logging.debug('all chunks found')
            else:
                logging.error('chunk_number ERROR')
                encmsg = encrypt_message([command, ['chunk_number ERROR', file_dir, file_name, chunk_number]], web_client)
                filecom.send_multipart([encmsg])
                continue
            # append chunks to new file
            new_file = open(file_path, 'w')
            new_file.write('')
            new_file.close()
            new_file = open(file_path, 'ab')
            for n in range(0, total_chunks):
                chunk_path = file_path + '.temp/' + str(n)
                tf = open(chunk_path, 'rb')
                new_file.write(tf.read())
                tf.close()
            new_file.close()
            new_file_length = str(os.path.getsize(file_path))
            logging.debug('new_file_lemgth = ' + new_file_length)
            new_file_hash = md5(file_path)
            logging.debug('file_hash = '  + str(file_hash))
            logging.debug('new_file_hash = ' + new_file_hash)
            f = open(file_path + '.temp/md5sum', 'r')
            file_hash = f.read()
            f.close()
            if new_file_hash == file_hash:
                encmsg = encrypt_message([command, ['FILE COMPLETE', file_dir, file_name, new_file_hash, new_file_length]], web_client)
                logging.info('file download complete')
                getting_file = False
                # remove temporary files
                files = os.listdir(file_path + '.temp/')
                logging.debug('removing files in ' + file_path + '.temp/')
                for i in files:
                    os.remove(file_path + '.temp/' + i)
                logging.debug('removing ' + file_path + '.temp')
                os.rmdir(file_path + '.temp')
            else:
                encmsg = encrypt_message([command, ['FILE ERROR', file_dir, file_name, new_file_hash, new_file_length]], web_client)
                logging.info('File Hash ERROR!')
            filecom.send_multipart([encmsg])
            
            
    
    if socks.get(brokercom) == zmq.POLLIN:
        try:
            message = brokercom.recv_multipart()
            logging.debug('encrypted_message = ' + str(encrypted_message))
            decrypted_message = decrypt_message(encrypted_message[0])
            logging.debug('decrypted_message = ' + decrypted_message)
        except Exception, e:
            runtime = str(time.time() - starttime)
            logging.critical('poller ERROR! runtime = ' + runtime + '   ' + str(e))
            sys.exit(0)
     
    if time.time() > heartbeat + 20:
        if time.time() > heartbeat_timeout:
            if os.system('ping -c 1 google.com') == 0:
                logging.debug('google.com ping OK')
                heartbeat_errors += 1
                if heartbeat_errors > 10:
                    logging.error('--------heartbeat timeout--------');
                    # Restart webcom_client
                    try:
                        supervisor_xml.supervisor.stopProcess('webcom_client')
                        logging.debug('restarting webcom_client supervisor process')
                    except:
                        logging.debug('starting webcom_clinet supervisor process')
                    supervisor_xml.supervisor.startProcess('webcom_client')
                    logging.debug('started webcom_client')
            else:
                logging.debug('google.com ping error')
        heartbeat = time.time()
        enc_hb = encrypt_message(heartbeat, '1')
        filecom.send_multipart([str(enc_hb)])
        messages_sent += 1
        logging.debug('heartbeat ' + str(messages_sent) + '  ' + str(enc_hb))
        
    
        




