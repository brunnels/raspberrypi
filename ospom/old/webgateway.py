#!/usr/bin/python

##################################################
# Request Temporary ID and Passsword from server #
##################################################
# All systems connect to the registration gateway
# at <serverip>:3001, and send up their system ID,
# system password, and local+outer IP addresses
#
# They are authorised and whitelisted to connect
# on :3002 for sensor updates, :3003 for image/video,
# and :3004 to receive commands from the server
##################################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################




import sys, os
import zmq, redis, django, time, pickle, ipgetter, socket, subprocess

# encryption functions
from Crypto.Cipher import AES
import base64

sys.path.append("/opt/aienv/aibox")
os.environ["DJANGO_SETTINGS_MODULE"] = "aibox.settings"
from django.contrib.auth.models import User
# flush database query from memory
from django.db import reset_queries
django.setup()
##from django.core.cache import caches
from dash.models import System

debug = 2

cache = redis.Redis(host='localhost', port=6379, db=0)

def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.connect(('<broadcast>', 0))
    return s.getsockname()[0]

ipi = getNetworkIp()
ipo = ipgetter.myip()

s = System.objects.all()[0]
sysid = str(s.ID)
if debug: print 'sysid = ' + sysid
syspass = str(s.pswd)
if debug: print 'syspass = ' + syspass

PADDING = '{'
DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

context = zmq.Context()
# Registration gateway
gateway = context.socket(zmq.REQ)
gateway.setsockopt(zmq.RCVTIMEO, 10000)
gateway.setsockopt(zmq.SNDTIMEO, 10000)
gateway.setsockopt(zmq.LINGER, 2000)
# start connections
gateway.connect("tcp://104.237.151.38:3001")

timenow = str(time.time())

sysinfo = {'ipi': ipi,
           'ipo': ipo,
           'timestamp': timenow}
psend = pickle.dumps(sysinfo)
##jsend = json.dumps(sysinfo)
cipher = AES.new(syspass)
encoded = EncodeAES(cipher, psend)
message = [sysid, encoded]

gateway.send_multipart(message)
print 'sent: ' + str(message)

erecvelio = gateway.recv()
##print 'received: ' + str(erecvelio)
precvelio = DecodeAES(cipher, erecvelio)
recvelio = pickle.loads(precvelio)
tempid = recvelio['tempid']
temppass = recvelio['temppass']
print 'Temporary ID: ' + tempid
print 'Temporary password: ' + temppass
# Update postgres database with new temporary ID and Password
s.tempid = tempid
s.temppass = temppass
s.save()
reset_queries()
# Update redis database with new temporary ID and Password
cache.set(sysid + 'tempid', tempid)
cache.set(sysid + 'temppass', temppass)

subprocess.call(["supervisorctl", "restart", "webcon"])

print 'exiting and closing all connections'
gateway.close()
context.term()
print 'connections closed'
sys.exit(0)
