#!/usr/bin/env python

##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


import zmq, time
from time import strftime

debug = 1

context = zmq.Context()
brokercon = context.socket(zmq.DEALER)
brokercon.connect('tcp://127.0.0.1:3007')

while 1:
    if debug: print strftime("%a, %d %b %Y %H:%M:%S")
    brokercon.send_multipart(['gcp00000', 'gcp0000011:1000!'])
    output = brokercon.recv_multipart()
    if debug: print output
    brokercon.send_multipart(['gls00000', 'gls00000:1000!'])
    output = brokercon.recv_multipart()
    if debug: print output
    brokercon.send_multipart(['gsd00000', 'gsd0000011:1000!'])
    output = brokercon.recv_multipart()
    if debug: print output
    time.sleep(55)
