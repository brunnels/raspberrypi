#!/usr/bin/env python

##############################################
#  ZeroMQ threads communicate with arduinos  #
##############################################
#
#  -Control Commands, DEALER, 127.0.0.1:3008
#      -Command Format
#          -['', <command>]
#      -Examples
#          -['', 'exit']	end program
#      -Reply Format
#          -[<reply>]
#
#  -Arduino Commands, DEALER, 127.0.0.1:3007
#      -Command Format
#          -[<groupID>, <command>]
#      -Examples
#          -['gsd00000', '10!']	get sensor data
#          -['gsd00000', '13!'] get actuator data
#      -Reply Format
#          -[<reply>]
#
##############################################
# Description
#
#  -send serial request to arduino, receive response
#  -send response back over zmq
#
#  -read/write to django database + redis
#    -redis contains key:<element ID>, value:<current sensor/actuator value>
#    -postgres stores element and group ID's, names, types, units, active status, etc.
#     Data is stored in the Element and Group models
#
#  -check bus for arduinos
#  -create new database entries if needed
#  
# 
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle, json

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupTypes, Element, ElementTypes, System
from django.db import reset_queries


'''
import xmlrpclib
supervisorduri = 'http://localhost:9003/RPC2'
supserv = xmlrpclib.Server(supervisorduri)
supserv.supervisor.startProcess('historycon')
'''


##debug = ['brokercom', 'arducom']
debug = []

cache = redis.Redis(host='localhost', port=6379, db=0)

sysid = ''
for s in System.objects.all():
    if s.default:
        sysid = s.ID
        break

context = zmq.Context()

arducom = context.socket(zmq.ROUTER)
arducom.setsockopt(zmq.RCVTIMEO, 5000)
arducom.setsockopt(zmq.SNDTIMEO, 5000)
arducom.setsockopt(zmq.HWM, 20)
arducom.setsockopt(zmq.LINGER, 2000)
arducom.bind('tcp://127.0.0.1:3006')

brokercom = context.socket(zmq.ROUTER)
brokercom.setsockopt(zmq.SNDTIMEO, 5000)
brokercom.setsockopt(zmq.RCVTIMEO, 5000)
brokercom.setsockopt(zmq.HWM, 20)
brokercom.setsockopt(zmq.LINGER, 2000)
brokercom.bind('tcp://127.0.0.1:3007')

mainpoller = zmq.Poller()
mainpoller.register(arducom, zmq.POLLIN)
mainpoller.register(brokercom, zmq.POLLIN)



while 1:
    socks = dict(mainpoller.poll(2000))
    
    if socks.get(brokercom) == zmq.POLLIN:
        try:
            clientid, groupid, command = brokercom.recv_multipart()
            if 'brokercom' in debug: print datetime.datetime.now().ctime() + '    received on brokercom: ' + str([clientid, groupid, command])
            if groupid in cache.smembers('congroups'):
                pid = cache.get(groupid + '_pid')
                arducom.send_multipart([pid, command, clientid])
                if 'brokercom' in debug: printdatetime.datetime.now().ctime() + '    sent to arducom: ' + str([groupid, command, clientid])
            else:
                brokercom.send_multipart([clientid, 'ERROR, groupid: ' + groupid + ' not found!'])
                print 'ERROR, groupid: ' + groupid + ' not found!'
        except Exception,e:
            print datetime.datetime.now().ctime() + '    brokercom ERROR: ' + str(e)
            
    if socks.get(arducom) == zmq.POLLIN:
        try:
            groupid, data, clientid = arducom.recv_multipart()
            if 'arducom' in debug: printdatetime.datetime.now().ctime() + '    received from thread: ' + str([groupid, data, clientid])
            brokercom.send_multipart([clientid, data])
            if 'arducom' in debug: print str(time.time()) + '    sent to frontend client: ' + str([clientid, data])
        except Exception,e:
            print datetime.datetime.now().ctime() + '    arducon ERROR: ' + str(e)
    sys.stdout.flush()

