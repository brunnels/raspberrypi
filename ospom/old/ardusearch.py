#!/usr/bin/env python

##############################################
#          Find connected Arduinos           #
##############################################
#
#  -check USB for open serial connections
#
#  -read/write to django database + redis
#    -redis contains key:<'elementid_port'>, value:<'/dev/ttyUSB..'>
#    -postgres stores Group ID's and active status
#
#  -if new Arduino found:
#    -create database entry
#    -create new supervisor configuration
#    -update supervisor
#    -start new process
# 
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


debug = True
import opm

import zmq, redis, time, datetime, xmlrpclib
import serial, glob, StringIO, string, pickle, json

from random import randint

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, GroupType, Sensor, SensorType, Actuator, ActuatorType, System
from django.db import reset_queries


server = xmlrpclib.Server('http://localhost:9003/RPC2')

cache = redis.Redis(host='localhost', port=6379, db=0)

context = zmq.Context()
ardusearch = context.socket(zmq.DEALER)
ardusearch.setsockopt(zmq.HWM, 10)
ardusearch.setsockopt(zmq.LINGER, 100)
ardusearch.setsockopt(zmq.RCVTIMEO, 3000)
ardusearch.setsockopt(zmq.SNDTIMEO, 3000)
ardusearch.bind('tcp://127.0.0.1:3016')
opm.dbug('opened ardusearch Zmq DEALER socket at localhost:3016 @ ' + str(time.time()))


def list_arduino_ports():
    return glob.glob('/dev/ttyACM[0-9]') + glob.glob('/dev/ttyUSB*')


def arduino_serial(serial, data):
    opm.dbug('data = ' + str(data))
    if len(data) > 1:
        groupid = data[0]
        opm.dbug('groupid = ' + groupid)
        message = data[1]
        opm.dbug('message = ' + message)
    else:
        message = data[0]
        opm.dbug('message = ' + message)
    resendtime = time.time() + 2
    opm.dbug('resendtime = ' + str(resendtime))
    timeout = time.time() + 12
    opm.dbug('timeout = ' + str(timeout))
    aresp = -1
    opm.dbug('aresp = ' + str(aresp))
    messageOK = False
    opm.dbug('messageOK = ' + str(messageOK))
    
    while serial.inWaiting():
        buffertrash = serial.readline()
        opm.dbug('buffertrash = ' + str(buffertrash))

    while time.time() < timeout and messageOK != True:
        if message == '0!':
            serial.write(message)
            aresp = serial.readlines()[0].strip()
            opm.dbug('aresp = ' + str(aresp))
            if aresp[:8] == aresp[9:]:
                opm.dbug('Arduino ID: ' + aresp[9:])
                messageOK = True
                aresp = aresp[9:]
            else:
                opm.dbug('ERROR1:' + aresp)
                serial.readline()
                
        else:
            opm.dbug('sending ' + str(groupid) + str(message))
            serial.write(str(groupid) + str(message))
            opm.dbug('wrote ' + str(groupid) + str(message))
            aresp = serial.readline().strip()
            opm.dbug('Arduino message: ' + aresp)
            arespcheck = False
            arespstart = time.time()
            while time.time() < arespstart + 2 and arespcheck == False:
                if aresp[:8] == groupid:
                    messageOK = True
                    arespcheck = True
                else:
                    opm.dbug('ERROR2:   ' + aresp)
                    aresp = serial.readline().strip()
    sys.stdout.flush()
    return aresp
    







nextcheck = time.time()
opm.dbug(datetime.datetime.now().ctime() + '    running find_arduinos()')

openports = list_arduino_ports()
opm.dbug('openports = ' + str(openports))

dbgroups = []
dbports = []
for i in Group.objects.all().filter(active=True):
    gid = i.ID.encode('ascii','ignore')
    dbgroups.append(gid)
    dbport = cache.get(gid + '_port')
    if dbport != None:
        dbports.append(cache.get(gid + '_port'))
    else:
        opm.dbug(gid + '_port not found in redis')
opm.dbug('dbgroups[] = ' + str(dbgroups))
opm.dbug('dbports[] = ' + str(dbports))

newports = list(openports)
opm.dbug('newports = ' + str(newports))
deactivatedports = []

sys.stdout.flush()

# Check if arduino groups found in databases are currently connected
if dbports:
    for thisport in dbports:
        opm.dbug('thisport = ' + thisport)
        if thisport in openports:
            opm.dbug(thisport + ' still connected')
            newports.remove(thisport)
        else:
            opm.dbug(str(thisport) + ' no longer connected')
            deactivatedports.append(thisport)
            
            opm.dbug('added ' + str(thisport) + ' to deactivatedports[]')
            cacheports = cache.keys('*_port')
            opm.dbug('cacheports = ' + str(cacheports))
            # check for redis port listings matching port no longer found on USB
            # delete redis key if found and set "active" flag in psql "Group" to False
            for k in cacheports:
                kport = cache.get(k)
                if kport == thisport:
                    thisgroup = k[:8]
                    opm.dbug('thisgroup = ' + thisgroup)
                    cache.delete(k)
                    opm.dbug('deleted ' + k + ' from redis')
                    cache.delete(thisgroup + '_pid')
                    opm.dbug('deleted ' + thisgroup + '_pid from redis')
                    cache.srem('congroups', thisgroup)
                    opm.dbug('removed ' + thisgroup + ' from congroups in redis')
                    g = Group.objects.get(ID=thisgroup)
                    g.active = False
                    g.save()
                    opm.dbug('set active = False in psql Group ' + thisgroup)
        opm.dbug('newports = ' + str(newports))
opm.dbug('deactivatedports[] = ' + str(deactivatedports))

sys.stdout.flush()

debug_pid = ''
debug_mygroup = ''

for system in System.objects.all():
    sysid = system.ID
    opm.dbug('sysid = ' + sysid)
    opm.dbug('Checking ports not found in Redis for new Arduinos')
    for p in newports:
        opm.dbug('checking port: ' + p)
        ser = serial.Serial(port=p, baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=2)
        mygroup = ''
        time.sleep(4)
        aresp = arduino_serial(ser, ['0!'])
        opm.dbug('aresp = ' + aresp)
        try:
            if len(aresp) == 8 and aresp[0] == 'g':
                opm.dbug('valid group')
                mygroup = str(aresp)
                gdes = mygroup[1:4]
                grouptype = ''
                for gtype in GroupType.objects.all():
                    if gtype.des == gdes:
                        grouptype = gtype.name
                        opm.dbug('found group type: "' + grouptype + '" in database')
                if grouptype == '':
                    opm.dbug('GroupType ' + gdes + ' not found in database')
                
                newgroup = 0
                if mygroup in dbgroups:
                    opm.dbug('found ' + mygroup + ' in dbgroups, setting active flag')
                    g = Group.objects.get(ID = mygroup)
                    g.active = True
                    g.save()
                else:
                    opm.dbug(mygroup + ' not found in dbgroups, creating new entry in psql')
                    g = Group(ID = mygroup)
                    g.gtype = grouptype
                    g.name = grouptype + ' ' + mygroup
                    g.active = True
                    g.save()
                    g.system.add(system)
                    g.save()
                    newgroup = 1
                opm.dbug('mygroup = ' + mygroup)
                cache.sadd('congroups', mygroup)
                if newgroup:
                    try:
                        opm.dbug('getting sensor IDs')
                        aresp = arduino_serial(ser, [mygroup, '12!'])
                        aresp = aresp.split('/')
                        opm.dbug('aresp = ' + str(aresp))
                        for edat in aresp[1].split(','):
                            edata = edat.split(':')
                            opm.dbug('edata = '+ str(edata))
                            eid = edata[0]
                            opm.dbug('eid = ' + str(eid))
                            edes = eid[1:3]
                            opm.dbug('edes = ' + str(edes))
                            elval = edata[1]
                            opm.dbug('elval = ' + str(elval))
                            ##opm.dbug('eid = ' + eid + ', val= ' + elval)
                            if not Sensor.objects.filter(ID=eid).exists():
                                opm.dbug('creating new database entry for Sensor: ' + eid)
                                e = Sensor(ID=eid)
                                try:
                                    sensortype = SensorType.objects.get(des=edes)
                                    opm.dbug('found sensortype ' + edes + ' in database')
                                    e.name = sensortype.name + ' ' + eid
                                    e.etype = sensortype.name
                                    e.eunits = sensortype.units
                                    opm.dbug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                                    g = Group.objects.get(ID = mygroup)
                                    e.group = g
                                    e.save()
                                except:
                                    opm.dbug('sensortype ' + edes + ' NOT FOUND IN DATABASE')
                                    opm.dbug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                                    g = Group.objects.get(ID = mygroup)
                                    e.group = g
                                    e.save()
                                
                        opm.dbug('getting actuator IDs')        
                        aresp = arduino_serial(ser, [mygroup, '14!'])
                        aresp = aresp.split('/')
                        if aresp[1] != '0':
                            for edat in aresp[1].split(','):
                                eid = edat.split(':')[0]
                                edes = eid[1:4]
                                elval = edat.split(':')[1]
                                opm.dbug('eid = ' + eid + ', val= ' + elval)
                                if not Actuator.objects.filter(ID=eid).exists():
                                    opm.dbug('creating new database entry for Actuator: ' + eid)
                                    e = Actuator(ID=eid)
                                    try:
                                        actuatortype = ActuatorType.objects.get(des=edes)
                                        opm.dbug('found actuatortype ' + edes + ' in database')
                                        e.name = actuatortype.name + ' ' + eid
                                        e.etype = actuatortype.name
                                        e.eunits = actuatortype.units
                                        opm.dbug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                                        g = Group.objects.get(ID = mygroup)
                                        e.group = g
                                        e.save()
                                    except:
                                        opm.dbug('actuatortype ' + edes + ' NOT FOUND IN DATABASE')
                                        opm.dbug('accessing Group: ' + mygroup + ', with name: ' + g.name)
                                        g = Group.objects.get(ID = mygroup)
                                        e.group = g
                                        e.save()
                    
                    except Exception, e:
                        print 'New Group ERROR: ' + str(e) + ' @ ' + str(time.time())
                        if Group.objects.filter(ID = mygroup).exists():
                            Group.objects.get(ID = mygroup).delete()
                            opm.dbug('Deleted Group: ' + mygroup)
                        ser.close()
                        cache.srem('congroups', mygroup)
                        continue
                
                try:
                    opm.dbug('getting active sensor IDs')
                    aresp = arduino_serial(ser, [mygroup, '10!'])
                    aresp = aresp.split('/')
                    opm.dbug('aresp = ' + str(aresp))
                    try:
                        for sensor in aresp[1].split(','):
                            sensor_data = sensor.split(':')
                            opm.dbug('sensor_data = ' + str(sensor_data))
                            sensor_id = sensor_data[0]
                            opm.dbug('sensor_id = ' + str(sensor_id))
                            sensor_des = sensor_id[1:3]
                            opm.dbug('sensor_des = ' + str(sensor_des))
                            sensor_val = sensor_data[1]
                            opm.dbug('sensor_val = ' + str(sensor_val))
                            cache.set(sensor_id, float(sensor_val))
                            s = Sensor.objects.get(ID=sensor_id)
                            s.active = True
                            s.save()
                            opm.dbug('set Sensor ' + sensor_id + ' to Active')
                    except Exception, e:
                        print str(time.time()) + '  ' + str(e)

                    opm.dbug('getting active actuator IDs')       
                    aresp = arduino_serial(ser, [mygroup, '13!'])
                    aresp = aresp.split('/')
                    try:
                        for actuator in aresp[1].split(','):
                            actuator_data = actuator.split(':')
                            opm.dbug('actuator_data = ' + str(actuator_data))
                            actuator_id = actuator_data[0]
                            opm.dbug('actuator_id = ' + str(actuator_id))
                            actuator_des = actuator_id[1:3]
                            opm.dbug('actuator_des = ' + str(actuator_des))
                            actuator_val = actuator_data[1]
                            opm.dbug('actuator_val = ' + str(actuator_val))
                            cache.set(actuator_id, float(actuator_val))
                            opm.dbug('eid = ' + eid + ', val= ' + elval)
                            a = Actuator.objects.get(ID=actuator_id)
                            a.active = True
                            a.save()
                            opm.dbug('set Actuator ' + actuator_id + ' to Active')
                    except Exception, e:
                        print str(time.time()) + '  ' + str(e)
                        
                except Exception, e:
                    print 'Group Element active ERROR: ' + str(e) + ' @ ' + str(time.time())
                    continue
                
                
                
                
                
                ser.close()
                time.sleep(2)
                cache.set(mygroup + '_port', p)
                cache.sadd('congroups', mygroup)
                 
                print 'Starting arducom.py supervisor process for group ' + mygroup + ' at port ' + p + ' @ ' + str(time.time())
                arducomID = ''
                for i in server.supervisor.getAllProcessInfo():
                    if i['statename'] != 'STARTING' and i['statename'] != 'RUNNING':
                        arducomID = i['group'] + ':' + i['name']
                        break
                server.supervisor.startProcess(arducomID)
                
                arduInfo = server.supervisor.getProcessInfo(arducomID)
                print 'Started supervisor process ' + arducomID + '\n'  + str(arduInfo)
                
                pid = str(arduInfo['pid'])
                cache.set(mygroup + '_pid', pid)

                timenow = time.time()
                timeout = timenow + 10
                msgOK = False
                while timenow < timeout and msgOK == False:
                    timenow = time.time()
                    try:
                        opm.dbug('Sending Group ID ' + mygroup + ' to arducom process at Zmq ID : ' + pid + ' @ ' + str(time.time()))
                        ardusearch.send_multipart([pid, mygroup])
                        msgresp = ardusearch.recv()
                        print 'msgresp = ' + str(msgresp)
                        if msgresp == mygroup: msgOK = True
                    except Exception, e:
                        print 'ardusearch error 1: ' + str(e)
                        
                print 'dbgroups: ' + str(dbgroups)
        except Exception,e:
            print 'ardusearch error 2: ' + str(e)
        ser.close()
    sys.stdout.flush()


for i in dbgroups:
    if i not in cache.smembers('congroups'):
        deactivatedports.append(i)
if debug and deactivatedports != []: opm.dbug('initial deactivatedports = ' + str(deactivatedports))
# check if port already deactivated
abdports = []
for g in Group.objects.filter(active=False):
    abdports.append(g.ID)
opm.dbug('abdports = ' + str(abdports))
olddeacports = []
for i in deactivatedports:
    if i in abdports:
        opm.dbug('found ' + i + 'in abdports')
        olddeacports.append(i)
    else:
        opm.dbug(i + ' not found in abdports')
for i in olddeacports:
   deactivatedports.remove(i)
opm.dbug('deactivatedports =  ' + str(deactivatedports))
# set Groups active = False
for i in deactivatedports:
    try:
        g = Group.objects.get(ID = i)
        g.active = False
        g.save()
        opm.dbug('deactivated ' + str(g.ID) + ', i = ' + str(i) + ' @ ' + str(time.time()))
    except Exception,e:
        print str(time.time()) + '    ' + str(e)

opm.dbug('ending ardusearch @ ' + str(time.time()))
sys.stdout.flush()


