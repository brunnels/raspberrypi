#!/usr/bin/env python

import serial, time

start_time = time.time()
serial_com = serial.Serial(port='/dev/ttyUSB0', baudrate=57600, bytesize=8, parity='N', stopbits=1, timeout=0.1)
print 'opened serial port in ' + str(time.time() - start_time)
while True:
    while serial_com.inWaiting():
        print 'buffer = ' + serial_com.readline() + ' ' + str(time.time() - start_time)
    # Send message to Arduino
    serial_com.write('0!')
    print 'sent message ' + str(time.time() - start_time)
    response = serial_com.readline()
    print response + ' ' + str(time.time() - start_time)
    if len(response) > 1:
        print response + ' ' + str(time.time() - start_time)
        break
    
serial_com.close()

