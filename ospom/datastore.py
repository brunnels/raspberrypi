#!/usr/bin/env python

###############################################
#       Historic Data Access & Control        #
#---------------------------------------------#
#
#  - Values stored in psql 'Sensor_History' model
#  - Access Redis for sensor values at programable intervals
#  - Intervals are specific to each element, and stored in Redis
#      Redis Keys:
#      - <sensor_ID>interval	interval in seconds as float
#      - <sensor_ID>		current value as float
#
#  - Entries are accessed through thier 'Sensor' model foriegn key
#      psql Models:
#      - Hisoric:
#        - element, timestamp, value
#      - Element:
#        - elementid
#
#  - ZeroMQ DEALER binds tcp127.0.0.1:3030 accepts requests for data / commands
#      Data Request Format:
#      - ['get', [<elementIDs>], <first_time>, <last_time>, <datapointQty>]
#      Interval Update Format:
#      - [<set>, <elementID>, <seconds>]
#
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle, sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)


# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import Group, Sensor, SensorHistory, SensorHistoryHourAvg
from django.db import reset_queries


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


cache = redis.Redis(host='localhost', port=6379, db=0)

# Make a list of all active sensors and fill with data storage times and intervals
sensors = {}
for group in Group.objects.filter(active=True):
    logging.debug('found group: ' + group.name + ' in postgres')
    for sensor in Sensor.objects.filter(group=group).filter(active=True):
        sensor_id = sensor.ID
        logging.debug('found active sensor: ' + sensor_id + ', checking redis for data storage interval')
        # Check sensor for how long to wait between storing readings in Redis
        storage_interval = cache.get(sensor_id + '_interval')
        time_now = time.time()
        next_storage_time = time_now
        # Default to 5 minute interval if none exists
        if not storage_interval:
            logging.debug('sensor: "' + sensor_id + '_interval" not found in redis, creating new entry, default = 300 (5 minutes)')
            storage_interval = 300
            # set default sensor storage interval in Redis
            cache.set(sensor_id + '_interval', storage_interval)
            logging.debug('Set ' + sensor_id + '_interval to ' + str(storage_interval) + 'sec in redis')
            next_storage_time = time_now + storage_interval
            logging.debug('next_storage_time = ' + str(next_storage_time))
        # Add next sensor storage time and storage interval to a dictionary containing all sensors for later access
        sensors[sensor.ID] = [next_storage_time, storage_interval]
        logging.debug('Added ' + str([next_storage_time, storage_interval]) + ' to sensors{}')
logging.debug('sensors = ' + str(sensors))

# Default refresh rate for sensor readings = 1000ms (1 second)
refreshtime = 1000
watchdogtime = time.time()

# hourly averages
hourly_avg_time = time.time()

context = zmq.Context()
historycom = context.socket(zmq.ROUTER)
historycom.setsockopt(zmq.RCVTIMEO, 20000)
historycom.setsockopt(zmq.SNDTIMEO, 20000)
historycom.setsockopt(zmq.RCVHWM, 10)
historycom.bind("tcp://127.0.0.1:3030")

arduino_broker = context.socket(zmq.DEALER)
arduino_broker.setsockopt(zmq.RCVTIMEO, 10000)
arduino_broker.setsockopt(zmq.SNDTIMEO, 10000)
arduino_broker.setsockopt(zmq.SNDHWM, 1)
arduino_broker.connect('tcp://127.0.0.1:3007')

zpoller = zmq.Poller()
zpoller.register(historycom, zmq.POLLIN)

while 1:
    reset_queries()
    timenow = time.time()
    for sensor in sensors:
        readtime = sensors[sensor][0]
        ##logging.debug(sensor + ', next read time = ' + str(readtime))
        if timenow > readtime:
            logging.debug('checking sensor ' + sensor)
            # Check for new sensor storage interval in Redis
            new_interval = float(cache.get(sensor + '_interval'))
            logging.debug('new_interval = ' + str(new_interval))
            old_interval = sensors[sensor][1]
            logging.debug('old_interval = ' + str(old_interval))
            if old_interval != new_interval:
                logging.info('setting sensors{' + str(sensor) + '} to reflect new interval')
                sensors[sensor][1] = new_interval
            else:
                logging.debug('storage interval unchanged @ ' + str(new_interval) + ' seconds')
            last_check_time = sensors[sensor][0]
            next_check_time = last_check_time + new_interval
            # Update sensor read time
            sensors[sensor][0] = next_check_time
            logging.debug('set next update time in sensors{' + str(sensor) + ': ' + str(next_check_time) +'}')
            try:
                # Get current sensor value from redis
                sensor_value = float(cache.get(sensor))
                logging.debug('got sensor_value for ' + str(sensor) + ' from redis: ' + str(sensor_value))
                # Check to make sure the value is a valid number
                if isinstance(sensor_value, (int, float)):
                    # Store value in 'Historic' psql model
                    first_time = timenow - new_interval
                    s = Sensor.objects.get(ID = str(sensor))
                    previous_data = SensorHistory.objects.filter(sensor=s).filter(timestamp__gt=first_time).filter(timestamp__lt=timenow)
                    if len(previous_data) > 0:
                        logging.info('found previous historic data for sensor')
                        continue
                    h = SensorHistory(sensor=s)
                    h.timestamp = timenow
                    h.value = sensor_value
                    h.save()
                    logging.info('wrote historic data to postgres for sensor ' + s.ID)
            except Exception,e:
                logging.error(' Cache ERROR! ' + str(e))
        # Clear postgres query
        reset_queries()
    
    # Every hour take the average value for sensors and store in postgres
    if timenow > hourly_avg_time:
        logging.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        logging.info('getting hourly averages')
        logging.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        hourly_avg_time = timenow + 60 * 60
        last_time = time.time()
        first_time = last_time - (60 * 60)
        all_sensors = Sensor.objects.filter(active=True)
        
        for s in all_sensors:
            try:
                previous_hour_averages = SensorHistoryHourAvg.objects.filter(sensor=sensor).filter(timestamp__gt=first_time).filter(timestamp__lt=last_time)
                if len(previous_hour_averages) > 0:
                    logging.info('found previous hourly average for sensor')
                    continue
                logging.debug('looking up sensor: ' + s.name)
                sensor = Sensor.objects.get(ID=s)
                logging.debug('sensor = ' + str(sensor))
                # Get all datapoint after 'first_time' and before 'last_time
                hdata = SensorHistory.objects.filter(sensor=sensor).filter(timestamp__gt=first_time).filter(timestamp__lt=last_time)
                
                logging.info('HOURLY hdata datapoints = ' + str(len(hdata)))
                sdata = []
                if len(hdata) < 2:
                    continue
                for h in hdata:
                    sdata.append([h.timestamp, h.value])
                # Sort all datapoints into chronological order
                sorted_data = sorted(sdata, key=lambda tstamp: tstamp[0])
                logging.debug('len(sorted_data) = ' + str(len(sorted_data)))
                current_time = sorted_data[0][0]
                logging.debug('current_time = ' + str(current_time))
                last_time = sorted_data[-1][0]
                logging.debug('last_time = ' + str(last_time))
                next_time = current_time + (60 * 60)
                while len(sorted_data) > 0:
                    hour_data = []
                    logging.debug('processing data between ' + str(current_time) + ' and ' + str(next_time))
                    logging.debug('next_time = ' + str(next_time))
                    while current_time < next_time:
                        try:
                            sensor_data = sorted_data.pop(0)
                        except:
                            break
                        sensor_value = sensor_data[1]
                        current_time = sensor_data[0]
                        hour_data.append(sensor_value)
                        logging.debug('added ' + str(sensor_value) + ' to hour_data @ ' + str(current_time))
                    current_time = next_time
                    next_time = next_time + (60 * 60)
                    ##logging.debug('hour_data = ' + str(hour_data))
                    hour_avg = 0
                    for value in hour_data:
                        hour_avg += value
                    hour_avg = hour_avg / len(hour_data)
                    logging.debug('hour_avg = ' + str(hour_avg))
                    # Store value in 'Historic' psql model
                    h = SensorHistoryHourAvg(sensor=sensor)
                    h.timestamp = current_time
                    h.value = hour_avg
                    h.save()
                    logging.info('wrote historic data ' + str(hour_avg) + ' @ ' + str(current_time) + ' to postgres')
                    time.sleep(0.1)
            except Exception, e:
                logging.error(e)

    # Every 60 seconds tell the arduinos to keep streaming thier sensor data
    if timenow > watchdogtime:
        watchdogtime += 60
        active_groups = Group.objects.filter(active=True)
        logging.debug(str(time.time()) + '  Active Groups:  ' + str(active_groups))
        for group in active_groups:
            try:
                group_id = group.ID
                if group_id == 'RasPi000':
                    continue
                logging.info('checking ' + group_id)
                output = ['']
                # Send '11:1000!' to the Arduino Group to have sensors stream data every 1000 miliseconds (1 second)
                arduino_broker.send_multipart([str(group_id), '11:1000!', ''])
                logging.debug('sent ' + str([str(group_id), '11:1000!', '']) + ' to arduino_broker')
                # Wait for response from Arduino
                response = arduino_broker.recv_multipart()
                logging.debug('response = ' + str(response))
                output = response[1]
                logging.info('output = ' + str(output))
                if output[:8] == str(group_id):
                    logging.debug('received valid response  ' + output)
                else:
                    logging.error(str(time.time()) + '  connection ERROR 1! group ' + str(group_id))
            except:
                logging.error('ERROR! group ' + str(group_id))

    # Check historycom for historic data retrieval requests
    socks = dict(zpoller.poll(refreshtime))
    ##logging.debug(str(time.time( )) + '  checking zpoller')
    if socks.get(historycom) == zmq.POLLIN:
        packet = historycom.recv_multipart()
        historycom_id = packet[0]
        logging.debug('historycom_id = ' + str(historycom_id))
        newmsg = pickle.loads(packet[1])
        logging.debug('newmsg = ' + str(newmsg))
        # 'get' requests ask for historic sensor data in the format: 
        #    ['get', [sensor IDs], <first datapoint>, <last datapoint>, <total datapoints>]
        webclient_id = newmsg.pop(0)
        if newmsg[0] == 'get':
            requested_sensors = newmsg[1]
            first_time = float(newmsg[2])
            last_time = float(newmsg[3])
            datapoints = int(newmsg[4])
            logging.info('Request for elements ' + str(requested_sensors) + ' data from ' + str(first_time) + ' to ' + str(last_time) + ' with ' + str(datapoints) + ' datapoints')
            ##reqdata = []
            reqdata = {}
            # Loop through all requested sensor IDs
            for e in requested_sensors:
                logging.info('Getting element ' + str(e) + ' data from ' + str(first_time) + ' to ' + str(last_time) + ' with ' + str(datapoints) + ' datapoints')
                
                try:
                    sensor = Sensor.objects.get(ID = e)
                    logging.debug('sensor = ' + str(sensor))
                    # Get all datapoint after 'first_time' and before 'last_time
                    hdata = []
                    hdata_points = 0
                    try:
                        hdata_points = SensorHistoryHourAvg.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time).count()
                        logging.debug('found ' + str(hdata_points) + ' hourly datapoints')
                    except:
                        logging.info('no hourly average datapoints found')
                    
                    if hdata_points >= datapoints:
                        hdata = SensorHistoryHourAvg.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time)
                        hdata_points = len(hdata)
                        logging.debug('Hourly hdata datapoints = ' + str(hdata_points))
                        
                        sdata = []
                        for h in hdata:
                            sdata.append([h.timestamp, h.value])
                        # Sort all datapoints into chronological order
                        indata = sorted(sdata, key=lambda tstamp: tstamp[0])
                        # Average readings to provide the requested number of datapoints
                        timestep = (last_time - first_time) / datapoints
                        msg = [hdata[0].value]
                        slicetime = [first_time, first_time + timestep]
                        for p in range(1, datapoints):
                            slicedata = []
                            for i in indata:
                                if i[0] > float(slicetime[1]):
                                    break
                                slicedata.append(i[1])
                                indata.remove(i)
                            if slicedata:
                                hv = 0
                                for d in slicedata:
                                    hv += float(d)
                                avgval = hv / len(slicedata)  
                                msg += [avgval]
                            else:
                                msg += [msg[-1]]
                            slicetime[0] += timestep
                            slicetime[1] += timestep
                        ##reqdata.append([e, msg])
                        reqdata[e] = msg
                    
                    else:
                        hdata = SensorHistory.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time)
                        logging.debug('hdata datapoints = ' + str(len(hdata)))
                        sdata = []
                        for h in hdata:
                            sdata.append([h.timestamp, h.value])
                        # Sort all datapoints into chronological order
                        indata = sorted(sdata, key=lambda tstamp: tstamp[0])
                        # Average readings to provide the requested number of datapoints
                        timestep = (last_time - first_time) / datapoints
                        try:
                            msg = [hdata[0].value]
                            slicetime = [first_time, first_time + timestep]
                            for p in range(1, datapoints):
                                slicedata = []
                                for i in indata:
                                    if i[0] > float(slicetime[1]):
                                        break
                                    slicedata.append(i[1])
                                    indata.remove(i)
                                if slicedata:
                                    hv = 0
                                    for d in slicedata:
                                        hv += float(d)
                                    avgval = hv / len(slicedata)  
                                    msg += [avgval]
                                else:
                                    msg += [msg[-1]]
                                slicetime[0] += timestep
                                slicetime[1] += timestep
                            reqdata[e] = msg
                        except:
                            msg = []
                            for n in range(datapoints):
                                msg.append(0)
                            reqdata[e] = msg
                except:
                    ##reqdata = ['ERROR = Group']
                    reqdata = {'ERROR': 'Group'}
            pdata = pickle.dumps(reqdata)
            # Send back all requested historic data
            historycom.send_multipart([historycom_id, webclient_id, pdata])
            logging.debug('sent  ' + str(reqdata))
        # 'set' requests provide a new data storage interval for a sensor with a format:
        #    ['set', 
        elif newmsg[0] == 'set':
            sensor_id = newmsg[1]
            logging.debug('sensor_id = ' + str(sensor_id))
            sensor_interval = newmsg[2]
            try:
                sensor = Sensor.objects.get(ID = sensor_id)
                logging.info('received request to change update interval on sensor ' + sensor_id + ' to ' + str(sensor_interval))
                cache.set(sensor_id + 'interval', sensor_interval)
                sensors[sensor_id] = [time.time(), sensor_interval]
                logging.debug('New sensors{} = ' + str(sensors))
                msg = 'set ' + sensor_id + ' update interval to ' + str(sensor_interval)
                historycom.send_multipart([historycom_id, webclient_id, msg])
            except Exception, e:
                msg = 'sensor set ERROR = ' + str(e)
                historycom.send_multipart([historycom_id, webclient_id, msg])
                logging.error('sensor set ERROR ' + str(e))
        else:
            historycom.send_multipart([historycom_id, 'ERROR = Message', newmsg])
            logging.error('Message Error')
    
    sys.stdout.flush()



