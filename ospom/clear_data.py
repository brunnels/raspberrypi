##############################################
#       License                              #
##############################################
#  Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  (GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  Contact:  Staff@OSPOM.com
##############################################


#!/usr/bin/env python

# Clears all Group, Sensor, Acduator and Redis data

import redis

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/aienv/aibox')
os.environ['DJANGO_SETTINGS_MODULE'] = 'aibox.settings'
django.setup()
from dash.models import Group, Sensor, Actuator

print 'Deleting Sensors'
for s in Sensor.objects.all():
    print str(s)
    s.delete()

print 'Deleting Actuators'
for a in Actuator.objects.all():
    print str(a)
    a.delete()

print 'Deleting Groups'
for g in Group.objects.all():
    print str(g)
    g.delete()

print 'deleting cache'
cache = redis.Redis()
for c in cache.keys():
    print str(c)
    cache.delete(c)

