#!/usr/bin/python

# saved

#######################################
#        Web to Box connection        #
#######################################
# Persistent Zeromq REQ REP socket
# Web Server binds to 104.237.151.38:3010 with ROUTER socket
# Box connects to web server with DEALER socket
#
# Command List:
#  - ['historic', 'set', <elementID>, <updateInterval>]		set historic data update interval for specified element
#  - ['historic', 'get', [<elementIDs>], <firstTime>, <lastTime>, <datapoints>]	get historic data for specified element
#  - ['group', 'getall', <groupID>]	get sensor data from specified arduino group
#######################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
######################################


import zmq, redis, django, time, pickle, subprocess, thread
import sys, logging, threading, os


import xmlrpclib
supervisor_xml = xmlrpclib.Server('http://localhost:9003/RPC2')


# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')

try:
    import psutil
except:
    logging.error('please install psutil')
    
try:
    from secrets import sudo_pass
except:
    logging.error('sudo_pass not found')

import git
if os.system('ping -c 1 google.com') == 0:
    logging.debug('google.com ping OK')
else:
    logging.debug('google.com ping error, waiting 10 seconds, then restarting')
    time.sleep(10)
    logging.debug('restarting')
    sys.exit(0)

try:
    logging.info('webcom:   UPDATE!!')
    g = git.cmd.Git('/opt/ospomenv/')
    output = g.pull()
    logging.debug('output = ' + output)
    if output != 'Already up-to-date.':
        try:
            supervisor_xml.supervisor.stopProcess('arduino_broker')
            logging.debug('restarting arduino_broker supervisor process')
        except:
            logging.debug('starting arduino_broker supervisor process')
        supervisor_xml.supervisor.startProcess('arduino_broker')
        logging.debug('started arduino_broker')
        sys.exit()
except Exception, e:
    logging.error(str(e))

import ipgetter
outter_ip = ipgetter.myip()

import base64

def pickle_message(msg, web_id):
    pickled_msg = pickle.dumps(msg)
    timenow = str(time.time())
    packet = {'msg': pickled_msg,
                    'webclientid': web_id}
    pickled_packet = pickle.dumps(packet)
    return pickled_packet

def load_message(msg):
    try:
        message = pickle.loads(msg)
    except Exception, e:
        message = msg
    return message

import socket 
def getNetworkIp():
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    soc.connect(('<broadcast>', 0))
    return soc.getsockname()[0]

local_ip = getNetworkIp()
logging.debug('local_ip = ' + str(local_ip))

# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import System, Group, GroupType, Sensor, SensorType, Actuator, ActuatorType
from dash.ospom_utils import clean_SensorHistory_time, del_SensorHistory_all
from django.db import reset_queries

cache = redis.Redis(host='localhost', port=6379, db=0)
webcom_pid = str(os.getpid())
cache.set('webcom_pid', webcom_pid)
logging.debug('pid = ' + webcom_pid)

# Get System info from postgres
system = System.objects.filter(default_system=True)[0]
system_id = str(system.ID)
logging.debug('system_id = ' + system_id)
syspass = str(system.password)
logging.debug('syspass = ' + syspass)
reset_queries()

import re
def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))
    
class log_reader(threading.Thread):
    def __init__(self, thread_id, log_path):
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.log_path = log_path
    def run(self):
        logging.debug('Starting thread ' + str(self.thread_id) + ', reading log file ' + self.log_path)
        try:
            logfile = open('/var/log/ospom/' + self.log_path,"r")
            logfile.seek(0,2)
            thread_time = time.time()
            global logger_timeout
            global webcom
            global temp_pass
            thread_ttl = logger_timeout - thread_time
            logging.debug('thread_ttl = ' + str(thread_ttl))
            while exitFlag == 0 and thread_time < logger_timeout:
                thread_time = time.time()
                line = logfile.readline().strip()
                if not line:
                    time.sleep(0.1)
                    continue
                else:
                    logging.debug('thread ' + str(self.thread_id) + ' sending ' + line + ' from file ' + self.log_path)
                    webcom.send_multipart(['3', pickle.dumps([temp_pass, [line, self.log_path]])])
        except Exception, e:
            logging.error(e)
        logging.debug('removing thread_id from log_ids')
        log_ids.remove(self.thread_id)
        logging.debug("Exiting " + self.thread_id)
    
def login():
    global temp_id
    global temp_pass
    ctx = zmq.Context.instance()
    client = ctx.socket(zmq.DEALER)
    client.setsockopt(zmq.LINGER, 2000)
    client.setsockopt(zmq.RCVTIMEO, 10000)
    client.setsockopt(zmq.SNDTIMEO, 10000)
    # Connect to registration gateway
    ##client.connect('tcp://104.237.151.38:3010')
    client.connect('tcp://127.0.0.1:3010')
    logging.debug('connected to registration gateway @ tcp://104.237.151.38:3010')
    # Request new temporary ID and password
    msg = ['login', system_id, syspass, outter_ip, local_ip]
    client.send_multipart(msg)
    logging.debug('sent ' + str(msg))
    logging.debug('waiting for temp ID')
    # Verify ID and password are valid
    try:
        response = client.recv_multipart()
        logging.debug('response = ' + str(response))
        temp_id = response[0]
        temp_pass = response[1]
        server_ip = response[2]
        if len(temp_id) == 16 and len(temp_pass) == 16 and is_valid_ip(server_ip) :
            logging.info('temp_id = ' + temp_id + '\n' + 'temp_pass = ' + temp_pass)
            cache.set('temp_id', temp_id)
            cache.set('temp_pass', temp_pass)
            cache.set('server_ip', server_ip)
        else:
            logging.critical('registration ERROR1, response = ' + str(response) + ' Exiting.')
            sys.exit()
    except Exception, e:
        logging.critical('registration ERROR2 ' + str(e) + ' Exiting.')
        sys.exit()
        
def run():
    # Get temporary ID from ZMQ
    global temp_id
    logging.debug('temp_id = ' + temp_id)
    # Get system info from redis
    global temp_pass
    temp_pass = cache.get('temp_pass')
    logging.debug('temp_pass = ' + temp_pass)
    
    # ZeroMQ settings
    ctx = zmq.Context.instance()
    image_push = ctx.socket(zmq.DEALER)
    image_push.setsockopt(zmq.RCVTIMEO, 5000)
    image_push.setsockopt(zmq.SNDTIMEO, 5000)
    image_push.setsockopt(zmq.LINGER, 2000)
    image_push.setsockopt(zmq.SNDHWM, 20)
    image_push.setsockopt(zmq.RCVHWM, 20)
    image_push.connect('tcp://127.0.0.1:3067') # Stunnel SSL
    # Connection to Arduino Groups
    ##arduino_broker = ctx.socket(zmq.ROUTER)
    arduino_broker = ctx.socket(zmq.DEALER)
    arduino_broker.setsockopt(zmq.RCVTIMEO, 5000)
    arduino_broker.setsockopt(zmq.SNDTIMEO, 5000)
    arduino_broker.setsockopt(zmq.LINGER, 2000)
    arduino_broker.setsockopt(zmq.SNDHWM, 20)
    arduino_broker.setsockopt(zmq.RCVHWM, 20)
    arduino_broker.connect('tcp://127.0.0.1:3007')
    # Historic data retrieval and settings 
    historycom = ctx.socket(zmq.DEALER)
    historycom.setsockopt(zmq.RCVTIMEO, 5000)
    historycom.setsockopt(zmq.SNDTIMEO, 5000)
    historycom.setsockopt(zmq.LINGER, 2000)
    historycom.setsockopt(zmq.SNDHWM, 10)
    historycom.setsockopt(zmq.RCVHWM, 10)
    historycom.connect('tcp://127.0.0.1:3030')
    # Web connection to OSPOM server
    global webcom
    webcom = ctx.socket(zmq.DEALER)
    webcom.setsockopt(zmq.IDENTITY, temp_id)
    logging.debug('set client socket identity to ' + temp_id)
    webcom.setsockopt(zmq.LINGER, 2000)
    webcom.setsockopt(zmq.RCVTIMEO, 10000)
    webcom.setsockopt(zmq.SNDTIMEO, 10000)
    webcom.connect('tcp://127.0.0.1:3011')
    logging.debug('connected to tcp://104.237.151.38:3011')
    
    # Move group commands to an import file !!!!!!!!!!!!!!!!!!!!!!!!
    group_commands = {}
    group_commands['getall'] = '10!'
    group_commands['lowcal'] = '38!'
    group_commands['direct'] = 'direct'

    # Verify connection status
    msg = 'login'
    webcom.send_multipart(['0', pickle.dumps([temp_pass, msg])])
    logging.debug('logging in to server')
    
    resp = webcom.recv_multipart()
    logging.debug('resp = ' + str(resp))
    
    # Decode response
    response = load_message(resp[0])
    logging.debug('response = ' + str(response))
    msg = response[2]
    logging.debug('msg = ' + str(msg))
    time_stamp = response[1]
    
    if msg != 'LOGIN OK':
        logging.critical('webcom client Login ERROR!')
        sys.exit()
    else:
        logging.info('connected to OSPOM server')
    
    # Start filecom_client
    try:
        supervisor_xml.supervisor.stopProcess('filecom_client')
        logging.debug('restarting filecom_client supervisor process')
    except:
        logging.debug('starting filecom_clinet supervisor process')
    supervisor_xml.supervisor.startProcess('filecom_client')
    logging.debug('started filecom_client')
    
    # Start sensor_push
    try:
        supervisor_xml.supervisor.stopProcess('sensor_push')
        logging.debug('restarting sensor_push supervisor process')
    except:
        logging.debug('starting sensor_push supervisor process')
    supervisor_xml.supervisor.startProcess('sensor_push')
    logging.debug('started sensor_push')
    
    # Start image_push
    try:
        supervisor_xml.supervisor.stopProcess('image_push')
        logging.debug('restarting image_push supervisor process')
    except:
        logging.debug('starting image_push supervisor process')
    try:
        supervisor_xml.supervisor.startProcess('image_push')
        logging.debug('started image_push')
    except:
        logging.info('image_push ERROR')
    
    try:  
        # Start rpi_worker
        try:
            supervisor_xml.supervisor.stopProcess('rpi_worker')
            logging.debug('restarting rpi_worker supervisor process')
        except:
            logging.debug('starting rpi_worker supervisor process')
        supervisor_xml.supervisor.startProcess('rpi_worker')
        logging.debug('started rpi_worker')
    except Exception, e:
        logging.debug(str(e))
        
    
    poller = zmq.Poller()
    poller.register(webcom, zmq.POLLIN)
    poller.register(arduino_broker, zmq.POLLIN)
    poller.register(historycom, zmq.POLLIN)
    poller.register(image_push, zmq.POLLIN)
    heartbeat = time.time()
    starttime = time.time()
    heartbeats_sent = 0
    heartbeat_check = True
    heartbeat_errors = 0
    google_ping_errors = 0
    thread_number = 0
    start_time = time.time()
    while True:
        if time.time() - 86400 > start_time:
            sys.exit()
        socks = dict(poller.poll(10000))
        ##logging.debug(str(time.time()))
        if socks.get(webcom) == zmq.POLLIN:
            try:
                # handle incoming messages from OSPOM server
                # Message format: [<web client ID>, <time stamp>, <message>]
                pickled_packet = webcom.recv_multipart()
                logging.debug('webcom:   pickled_packet = ' + str(pickled_packet))
                packet = load_message(pickled_packet[0])
                logging.debug('webcom:   packet = ' + str(packet))
                web_client = packet[0]
                time_stamp = packet[1]
                message = packet[2]
                logging.debug('webcom:   message = ' + str(message))
                try:
                    request = message[1:]
                    request.insert(0, web_client)
                    logging.debug('webcom:   request = ' + str(request))
                except:
                    request = []
                # Process the message
                if message == 'PING' or message == 'ping':
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'PONG'])])
                    logging.debug('webcom:   PONG')
                elif message == 'UPDATE' or message == 'update':
                    logging.info('webcom:   UPDATE!!')
                    try:
                        g = git.cmd.Git('/opt/ospomenv/') 
                        output = g.pull()
                        logging.debug('output = ' + output)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(output)])])
                        if output != 'Already up-to-date.':
                            try:
                                supervisor_xml.supervisor.stopProcess('arduino_broker')
                                logging.debug('restarting arduino_broker supervisor process')
                            except:
                                logging.debug('starting arduino_broker supervisor process')
                            supervisor_xml.supervisor.startProcess('arduino_broker')
                            logging.debug('started arduino_broker')
                            sys.exit()
                    except Exception, e:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'ERROR:  ' + str(e)])])
                elif message == 'STATUS' or message == 'status':
                    status = pickle.dumps(supervisor_xml.supervisor.getAllProcessInfo())
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, status])])
                    logging.debug('webcom:   Sent Supervisor status to server')
                
                # List available wifi access points
                elif message == 'WIFI_LIST' or message == 'wifi_list':
                    wifi_info = subprocess.check_output("nmcli d wifi list", shell=True).split('\n')[1:]
                    wifi_data = {}
                    for ap in wifi_info[:-1]:
                        ap_info = ap.split("'")[1:]
                        ap_id = ap_info.pop(0)
                        logging.debug('ap_id = ' + str(ap_id))
                        ap_info = ap_info[0].split()
                        logging.debug('apinfo = ' + str(ap_info))
                        wifi_data[ap_id] = {
                            'ap_bssid': ap_info[0],
                            'ap_mode': ap_info[1],
                            'ap_freq': ap_info[2],
                            'ap_frequ': ap_info[3],
                            'ap_rate': ap_info[4],
                            'ap_rateu': ap_info[5],
                            'ap_signal': ap_info[6],
                            'ap_active': ap_info[8],
                            'ap_secur':ap_info[7]
                        }
                    logging.debug('wifi_data = ' + str(wifi_data))
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(wifi_data)])])
                    logging.debug('webcom:   Sent wifi ap list to server')
                
                # Connect to wifi access point
                elif message[0] == 'WIFI_CONNECT' or message[0] == 'wifi_connect':
                    network_info = subprocess.check_output("nmcli d list", shell=True).split('\n')
                    network_data = {}
                    wifi_iface = False
                    for n in network_info:
                        if n[:15] == 'GENERAL.DEVICE:':
                            n_device = n.split()[1]
                            if n_device[:4] == 'wlan':
                                logging.debug('found wifi ' + n_device)
                                wifi_iface = n_device
                        if n[:13] == 'AP[1].ACTIVE:' and wifi_iface:
                            active_status = n.split()[1]
                            if active_status == 'yes':
                                continue
                            else:
                                wifi_iface = False
                    if wifi_iface:
                        disconnect_status = subprocess.check_output("echo " + sudo_pass + " | sudo -S nmcli d disconnect iface " + wifi_iface, shell=True).split('\n')[1:]
                        logging.debug('disconnect_status = ' + str(disconnect_status))
                    
                    connect_ssid = message[1]
                    connect_pass = message[2]    
                    connect_status = subprocess.check_output("echo " + sudo_pass + " | sudo -S nmcli d wifi connect '" + connect_ssid + "' password '" + connect_pass + "'", shell=True).split('\n')[1:]
                    logging.debug('connect_status = ' + str(connect_status))
                    if connect_status == []:
                        connect_status = 'WIFI OK'
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(connect_status)])])
                    logging.debug('webcom:   Sent wifi connection info to server')
                
                
                # Restart webcom_client
                elif message == 'RESTART_WEBCOM' or message == 'restart_webcom':
                    logging.info('webcom:   received RESTART_WEBCOM request')
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'RESTART_WEBCOM OK'])])
                    sys.exit()
                
                # Restart webcom_client
                elif message == 'RESTART_IMAGEPUSH' or message == 'restart_imagepush':
                    logging.info('webcom:   received RESTART_IMAGEPUSH request')
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'RESTART_IMAGEPUSH OK'])])
                    try:
                        supervisor_xml.supervisor.stopProcess('image_push')
                        logging.debug('restarting image_push supervisor process')
                    except:
                        logging.debug('starting image_push supervisor process')
                    try:
                        supervisor_xml.supervisor.startProcess('image_push')
                        logging.debug('started image_push')
                    except:
                        logging.info('image_push ERROR')
                    
                # Restart filecom_client
                elif message == 'RESTART_FILECOM' or message == 'restart_filecom':
                    logging.info('webcom:   received RESTART_FILECOM request')
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'RESTART_FILECOM OK'])])
                    try:
                        supervisor_xml.supervisor.stopProcess('filecom_client')
                        logging.debug('restarting filecom_client supervisor process')
                    except:
                        logging.debug('starting filecom_client supervisor process')
                    try:
                        supervisor_xml.supervisor.startProcess('filecom_client')
                        logging.debug('started filecom_client')
                    except:
                        logging.info('filecom_client ERROR')
                    
                # Restart System
                elif message[0] == 'RESTART' or message[0] == 'restart':
                    logging.info('webcom:   received RESTART request')
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'OK'])])
                    try:
                        os.system('echo %s|sudo -S %s' % (sudo_pass, 'sudo reboot'))
                    except:
                        try:
                            sudoPassword = message[1]
                            os.system('echo %s|sudo -S %s' % (sudoPassword, 'sudo reboot'))
                        except Exception, e:
                            logging.error(str(e))
                # Run sudo Command in a Thread
                elif message[0] == 'RUN_SUDO_THREAD' or message[0] == 'run_sudo_thread':
                    logging.info('webcom:   received RUN_SUDO_THREAD request')
                    command = message[1]
                    try:
                        def run_command(supass, cmd):
                            p = os.system('echo %s|sudo -S %s' % (supass, cmd))
                        thread = threading.Thread(target=run_command, args=(sudo_pass, command))
                        thread.start()
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'RUN_THREAD OK'])])
                    except Exception, e:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(e)])])
                
                # Run Command in a Thread
                elif message[0] == 'RUN_THREAD' or message[0] == 'run_thread':
                    logging.info('webcom:   received RUN_THREAD request')
                    command = message[1]
                    try:
                        def run_command(cmd):
                            p = os.system(cmd)
                        thread = threading.Thread(target=run_command, args=(command))
                        thread.start()
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'RUN_THREAD OK'])])
                    except Exception, e:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(e)])])
                
                # Run sudo Command  
                elif message[0] == 'RUN_SUDO' or message[0] == 'run_sudo':
                    logging.info('webcom:   received RUN_SUDO request')
                    command = message[1]
                    p = os.system('echo %s|sudo -S %s' % (sudo_pass, command))
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(p)])])
                
                # Run Command  
                elif message[0] == 'RUN' or message[0] == 'run':
                    logging.info('webcom:   received RUN request')
                    command = message[1]
                    ##p = os.system(command)
                    p = subprocess.check_output(command, shell=True)
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(p)])])
                    
                # clean historic data
                elif message[0] == 'CLEAN_SENSORHISTORY_TIME' or message[0] == 'clean_sensorhistory_time':
                    logging.info('webcom:   received CLEAN_SENSORHISTORY_TIME request')
                    sensors = message[1]
                    time_start = message[2]
                    time_end = message[3]
                    thread = threading.Thread(target=clean_SensorHistory_time, args=(sensors, time_start, time_end))
                    thread.start()
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'CLEAN_SENSORHISTORY_TIME OK'])])
                    
                # Pip Install
                elif message[0] == 'PIP_INSTALL' or message[0] == 'pip_install':
                    logging.info('webcom:   received PIP_INSTALL request')
                    package = message[1]
                    p = os.system('pip install -t /opt/ospomenv/lib/python2.7/site-packages/ ' + package)
                    logging.debug('webcom:    ' + str(p))
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(p)])])

                # Requests for Image Settings
                elif message[0] == 'image_push':
                    logging.info('webcom:   received image_push request')
                    try:
                        image_push.send(pickle.dumps(message[1:]))
                        output = image_push.recv()
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(output)])])
                    except Exception, e:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(e)])])
                elif message == 'HEARTBEAT OK':
                    heartbeat_check = True
                    heartbeat_errors = 0
                    
                # Request for historic data, or to set data storage interval
                elif message[0] == 'historic':
                    historycom.send(pickle.dumps(request))
                    
                # General System requests
                elif message[0] == 'system':
                    if message[1] == 'set_sensor_update':
                        cache.set('sensor_update_interval', message[2])
                        logging.debug('webcom:   set sensor update interval to ' + str(message[2]))
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps('set sensor update interval to ' + str(message[2]))])])
                    else:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps('Unknown system request ' + str(message[1]))])])
                # Change sensor & actuator info
                elif message[0] == 'SET_ELEMENT_INFO' or message[0] == 'set_element_info':
                    resp = ''
                    element_id = message[1]
                    if element_id[0] == 's':
                        try:
                            sensor = Sensor.objects.get(ID=element_id)
                            sensor_info = message[2]
                            logging.debug('sensor_info = ' + str(sensor_info))
                            if 'name' in sensor_info:
                                sensor.name = sensor_info['name']
                                sensor.save()
                                logging.debug('set sensor name to ' + sensor_info['name'])
                            if 'units' in sensor_info:
                                sensor.sensor_units = sensor_info['units']
                                sensor.save()
                                logging.debug('set sensor sensor_units to ' + sensor_info['units'])
                            logging.debug('saved sensor info in postgres')
                            resp = 'set_element_info OK'
                        except Exception, e:
                            logging.error('set_element_info ERROR ' + str(e))
                            resp = str(e)
                    if element_id[0] == 'a':
                        try:
                            actuator = Actuator.objects.get(ID=element_id)
                            actuator_info = message[2]
                            logging.debug('actuator_info = ' + str(actuator_info))
                            if 'name' in actuator_info:
                                actuator.name = actuator_info['name']
                                actuator.save()
                                logging.debug('set actuator name to ' + actuator_info['name'])
                            if 'units' in actuator_info:
                                actuator.actuator_units = actuator_info['units']
                                actuator.save()
                                logging.debug('set actuator actuator_units to ' + actuator_info['units'] + ' in psql')
                            
                            if 'max' in actuator_info:
                                cache.set(actuator.ID + '_max', actuator_info['max'])
                                logging.debug('set actuator actuator_max to ' + actuator_info['max'] + ' in redis')
                            if 'min' in actuator_info:
                                cache.set(actuator.ID + '_min', actuator_info['min'])
                                logging.debug('set actuator actuator_min to ' + actuator_info['min'] + ' in redis')
                            if 'onoff' in actuator_info:
                                cache.set(actuator.ID + '_onoff', actuator_info['onoff'])
                                logging.debug('set actuator actuator_onoff to ' + str(actuator_info['onoff']) + ' in redis')
                            logging.debug('saved actuator info in postgres and redis')
                            resp = 'set_element_info OK'
                        except Exception, e:
                            logging.error('set_element_info ERROR ' + str(e))
                            resp = str(e)
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, resp])])
                
                # Change Arduino Group info
                elif message[0] == 'SET_GROUP_INFO' or message[0] == 'set_group_info':
                    resp = ''
                    group_id = message[1]
                    try:
                        group = Group.objects.get(ID=group_id)
                        group_info = message[2]
                        logging.debug('group_info = ' + str(group_info))
                        if 'name' in group_info:
                            group.name = group_info['name']
                            group.save()
                            logging.debug('set group name to ' + group_info['name'])
                        logging.debug('saved group info in postgres')
                        resp = 'set_group_info OK'
                    except Exception, e:
                        logging.error('set_group_info ERROR ' + str(e))
                        resp = str(e)
                    webcom.send_multipart([web_client, pickle.dumps([temp_pass, resp])])
                
                # Requests for Arduino Group info
                elif message[0] == 'group':
                    # Add the web client ID to the message for response routing
                    action = request[1]
                    logging.debug('webcom:   action = ' + action)
                    if action == 'list_active_groups':
                        system = System.objects.get(ID=system_id)
                        group_ids = []
                        for group in Group.objects.filter(system=system).filter(active=True):
                            group_ids.append(str(group.ID))
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(group_ids)])])
                    elif action == 'list_active_sensors':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        sensor_ids = []
                        for sensor in Sensor.objects.filter(group=group).filter(active=True):
                            sensor_ids.append(sensor.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(sensor_ids)])])
                    elif action == 'list_all_sensors':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        sensor_ids = []
                        for sensor in Sensor.objects.filter(group=group):
                            sensor_ids.append(sensor.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(sensor_ids)])])
                    elif action == 'list_active_actuators':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        actuator_ids = []
                        for actuator in Actuator.objects.filter(group=group).filter(active=True):
                            actuator_ids.append(actuator.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(actuator_ids)])])
                    elif action == 'list_all_actuators':
                        group_id = request[2]
                        group = Group.objects.get(ID=group_id)
                        actuator_ids = []
                        for actuator in Actuator.objects.filter(group=group):
                            actuator_ids.append(actuator.ID)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(actuator_ids)])])
                    elif action == 'direct':
                        command = request[2]
                        logging.debug('webcom:   command = ' + command)
                        group_id = request[3]
                        logging.debug('webcom:   group_id = ' + group_id)
                        arduino_broker.send_multipart([group_id, command, web_client])
                
                # Log stream requests
                elif message[0] == 'get_logs':
                    exitFlag = 0
                    try:
                        global logger_timeout
                        logger_timeout = time.time() + 30
                        log_path = message[1]
                        log_id = log_path.split('/')[-1]
                        if log_id not in log_ids:
                            log_ids.append(log_id)
                            log_thread = log_reader(log_id, log_path)
                            log_thread.start()
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, 'logging OK'])])
                    except Exception, e:
                        logging.error(e)
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, str(e)])])
                        
                # Log file list requests
                elif message == 'get_log_list':
                    dir_name = '/var/log/ospom'
                    logging.debug('dir_name = ' + dir_name)
                    log_list = []
                    error_msg = ''
                    try:
                        for path, dirs, files in os.walk(dir_name):
                            current_dir = path.replace(dir_name, '')
                            logging.debug('current_dir = ' + current_dir)
                            logging.debug('files = ' + str(files))
                            for f in files:
                                log_list.append(current_dir + '/' + f)
                        logging.debug('log_list = ' + str(log_list))
                    except Exception, e:
                        logging.error(e)
                        error_msg += str(e)
                    if error_msg == '':
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(log_list)])])
                    else:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, error_msg])])
                        
                # File requests
                elif message[0] == 'get_file':
                    file_name = message[1]
                    error_msg = ''
                    file_text = ''
                    try:
                        f = open('/opt/ospomenv/ospom/' + file_name, 'r')
                        file_text = f.read()
                        f.close()
                    except Exception, e:
                        error_msg += str(e)
                        try:
                            f = open('/opt/ospomenv/local/' + file_name, 'r')
                            file_text = f.read()
                            f.close()
                        except Exception, e:
                            error_msg += str(e)
                    if file_text == '':
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, error_msg])])
                    else:
                        webcom.send_multipart([web_client, pickle.dumps([temp_pass, file_text])])
                    
                    
                    
                reset_queries()
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('webcom:   poller ERROR1! runtime = ' + runtime + '   ' + str(e))
                sys.exit()
        
        if socks.get(arduino_broker) == zmq.POLLIN:
            try:
                response = arduino_broker.recv_multipart()
                logging.debug('arduino_broker:   response = ' + str(response))
                web_client = response[0]
                logging.debug('arduino_broker:   web_client = ' + web_client)
                data = response[1]
                logging.debug('arduino_broker:   data = ' + data)
                webcom.send_multipart([web_client, pickle.dumps([temp_pass, pickle.dumps(data)])])
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('arduino_broker:   poller ERROR2! runtime = ' + runtime + '   ' + str(e))
                ##sys.exit()
                ##logging.error('restarting system')
                ##os.system('echo %s|sudo -S %s' % (sudo_pass, 'sudo reboot'))
                
                
        if socks.get(historycom) == zmq.POLLIN:
            try:
                packet = historycom.recv_multipart()
                logging.debug('historycom:   packet = ' + str(packet))
                web_client = packet[0]
                data = packet[1]
                webcom.send_multipart([web_client, pickle.dumps([temp_pass, data])])
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('historycom:   poller ERROR3! runtime = ' + runtime + '   ' + str(e))
                sys.exit()
                
        if socks.get(image_push) == zmq.POLLIN:
            try:
                data = image_push.recv()
                logging.debug('image_push:   data = ' + str(data))
                web_client = '2'
                webcom.send_multipart([web_client, pickle.dumps([temp_pass, data])])
            except Exception, e:
                runtime = str(time.time() - starttime)
                logging.critical('image_push:   poller ERROR4! runtime = ' + runtime + '   ' + str(e))
                ##sys.exit()
        
        if time.time() > heartbeat + 20:
            heartbeat = time.time()
            if heartbeat_check == False:
                if os.system('ping -c 1 google.com') == 0:
                    heartbeat_errors += 1
                    google_ping_errors = 0
                    if heartbeat_errors > 10:
                        logging.debug('error limit exeded, exiting')
                        sys.exit()
                else:
                    logging.debug('google.com not responding')
                    google_ping_errors += 1
                    if google_ping_errors > 20:
                        network_data = subprocess.check_output('nmcli device status', shell=True).split('\n')[1:-1]
                        logging.debug('network_data = ' + str(network_data))
                        ethernet_connected = False
                        wifi_connected = False
                        wifi_ssid = ''
                        wifi_name = ''
                        for d in network_data:
                            device_data = d.split()
                            device_name = device_data[0]
                            device_type = device_data[1]
                            device_state = device_data[2]
                            if device_name[:3] == 'eth' and device_state == 'connected':
                                ethernet_connected = True
                            elif device_name[:4] == 'wlan':
                                wifi_connected = True
                                wifi_name = device_name
                        if ethernet_connected == False:
                            ##logging.debug('restarting wifi ' + wifi_name)
                            ##resp = subprocess.check_output("echo " + sudo_pass + " | sudo -S nmcli d disconnect iface " + wifi_name, shell=True)
                            ##logging.debug(str(resp))
                            ##resp = subprocess.check_output("echo " + sudo_pass + " | sudo -S nmcli d wifi connect " + wifi_ssid + " --nowait", shell=True)
                            ##logging.debug(str(resp))
                            ##time.sleep(2)
                            logging.error('restarting system')
                            os.system('echo %s|sudo -S %s' % (sudo_pass, 'sudo reboot'))
                            
                        
                    
            else:
                heartbeat_errors = 0
                google_ping_errors = 0
            heartbeat = time.time()
            webcom.send_multipart(['1', pickle.dumps([temp_pass, 'HEARTBEAT'])])
            heartbeats_sent += 1
            logging.debug('heartbeat ' + str(heartbeats_sent))
            heartbeat_check = False
            


exitFlag = 0
logger_timeout = time.time()
log_ids = []
##temp_pass = ''

if __name__ == '__main__':
    logging.info(str(time.time()) + ' starting webcom_client')
    login()
    
    run()

