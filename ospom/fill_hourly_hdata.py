#!/usr/bin/env python

###############################################
#       Historic Data Access & Control        #
#---------------------------------------------#
#
#  - Values stored in psql 'Sensor_History' model
#  - Access Redis for sensor values at programable intervals
#  - Intervals are specific to each element, and stored in Redis
#      Redis Keys:
#      - <sensor_ID>interval	interval in seconds as float
#      - <sensor_ID>		current value as float
#
#  - Entries are accessed through thier 'Sensor' model foriegn key
#      psql Models:
#      - Hisoric:
#        - element, timestamp, value
#      - Element:
#        - elementid
#
#  - ZeroMQ DEALER binds tcp127.0.0.1:3030 accepts requests for data / commands
#      Data Request Format:
#      - ['get', [<elementIDs>], <first_time>, <last_time>, <datapointQty>]
#      Interval Update Format:
#      - [<set>, <elementID>, <seconds>]
#
#
##############################################
# License                               
#                                             
#   Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   (GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Contact:  Staff@OSPOM.com
##############################################


import zmq, redis, time, datetime, pickle, sys, logging

# Set logging level
if '-v' in sys.argv:
    log_level = logging.INFO
elif '-vv' in sys.argv:
    log_level = logging.DEBUG
else:
    log_level = logging.ERROR
logging.basicConfig(level=log_level)


# set python path, import django app settings
import sys, os, django
sys.path.append('/opt/ospomenv/ospom_django')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ospom_django.settings'
django.setup()
from dash.models import Group, Sensor, SensorHistory, SensorHistoryHourAvg
from django.db import reset_queries


# hourly averages
hourly_avg_time = time.time()

timenow = time.time()

logging.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
logging.debug('getting hourly averages')
logging.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

hourly_avg_time = 60 * 60

end_time = time.time()
start_time = end_time - 3000000

all_sensors = Sensor.objects.filter(active=True)

for s in all_sensors:
    logging.debug('checking sensor ' + str(s.ID))
    last_time = start_time + hourly_avg_time
    first_time = start_time
    current_time = last_time
    last_val = 0
    while current_time < end_time:
        try:
            logging.debug('looking up sensor: ' + s.name)
            sensor = Sensor.objects.get(ID=s)
            logging.debug('sensor = ' + str(sensor))
            # Check for existing hourly averages
            hourlydata = SensorHistoryHourAvg.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time)
            if len(hourlydata) == 0:
                # Get all datapoint after 'first_time' and before 'last_time
                hdata = SensorHistory.objects.filter(sensor=sensor).filter(timestamp__gt=first_time, timestamp__lt=last_time)
                
                logging.debug('HOURLY hdata datapoints = ' + str(len(hdata)))
                sdata = []
                if len(hdata) < 1:
                    # Store value in 'Historic' psql model
                    h = SensorHistoryHourAvg(sensor=sensor)
                    h.timestamp = current_time
                    h.value = last_val
                    h.save()
                    logging.debug('wrote historic data 0 @ ' + str(current_time) + ' to postgres')
                else:
                    for h in hdata:
                        sdata.append([h.timestamp, h.value])
                    # Sort all datapoints into chronological order
                    sorted_data = sorted(sdata, key=lambda tstamp: tstamp[0])
                    logging.debug('len(sorted_data) = ' + str(len(sorted_data)))
                    current_time = sorted_data[0][0]
                    logging.debug('current_time = ' + str(current_time))
                    last_time = sorted_data[-1][0]
                    logging.debug('last_time = ' + str(last_time))
                    next_time = current_time + (60 * 60)
                    while len(sorted_data) > 0:
                        hour_data = []
                        logging.debug('processing data between ' + str(current_time) + ' and ' + str(next_time))
                        logging.debug('next_time = ' + str(next_time))
                        while current_time < next_time:
                            try:
                                sensor_data = sorted_data.pop(0)
                            except:
                                break
                            sensor_value = sensor_data[1]
                            current_time = sensor_data[0]
                            hour_data.append(sensor_value)       
                            logging.debug('added ' + str(sensor_value) + ' to hour_data @ ' + str(current_time))
                        current_time = next_time
                        next_time = next_time + (60 * 60)
                        logging.debug('hour_data = ' + str(hour_data))
                        hour_avg = 0
                        for value in hour_data:
                            hour_avg += value
                        hour_avg = hour_avg / len(hour_data)
                        logging.debug('hour_avg = ' + str(hour_avg))
                        last_val = hour_avg
                        # Store value in 'Historic' psql model
                        h = SensorHistoryHourAvg(sensor=sensor)
                        h.timestamp = current_time
                        h.value = hour_avg
                        h.save()
                        logging.debug('wrote historic data ' + str(hour_avg) + ' @ ' + str(current_time) + ' to postgres')
                        time.sleep(0.25)
            else:
                logging.debug('hourly data found')
        except Exception, e:
            logging.error(e)
        
        first_time = last_time
        current_time += hourly_avg_time
        last_time = current_time
        time.sleep(0.1)


